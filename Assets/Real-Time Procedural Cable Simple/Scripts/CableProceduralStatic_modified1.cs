﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(LineRenderer))]
[ExecuteAlways]
public class CableProceduralStatic_modified1 : MonoBehaviour
{

	LineRenderer line;

	// The Start of the cable will be the transform of the GameObject that has this component.
	// The Transform of the Gameobject where the End of the cable is. This needs to be assigned in the inspector.
	[SerializeField] Transform startTransform, endPointTransform;

	// Number of points per meter
	[SerializeField, Tooltip("Number of points per unit length, using the straight line from the start to the end transform.")] float pointDensity = 3;

	// How much the cable will sag by.
	[SerializeField] float sagAmplitude = 1;
	public float sagAmplitude_div_factor = 300;//3000 for banana pins scale is 0.0001 else keep it 300
	// These are used later for calculations
	int pointsInLineRenderer;
	Vector3 vectorFromStartToEnd;
	Vector3 sagDirection;



	void Start () 
	{
		//line = GetComponent<LineRenderer>();

		//if (!endPointTransform)
		//{
		//	Debug.LogError("No Endpoint Transform assigned to Cable_Procedural component attached to " + gameObject.name);
		//	return;
		//}

		//// Get direction Vector.
		//vectorFromStartToEnd = endPointTransform.position - transform.position;
		//// Setting the Start object to look at the end will be used for making the wind be perpendicular to the cable later.
		//transform.forward = vectorFromStartToEnd.normalized;
		//// Get number of points in the cable using the distance from the start to end, and the point density
		//pointsInLineRenderer = Mathf.FloorToInt(pointDensity * vectorFromStartToEnd.magnitude);
		//// Set number of points in line renderer
		//line.positionCount = pointsInLineRenderer;	

		//// The Direction of SAG is the direction of gravity
		//sagDirection = Physics.gravity.normalized;

		//Draw();

		DrawLineBWpoints();
	}
    private void Update()
    {
        if(startTransform.transform.hasChanged)
        {
			startTransform.transform.hasChanged = false;
			DrawLineBWpoints();
        }
		else if(endPointTransform.hasChanged)
        {
			endPointTransform.hasChanged = false;
			DrawLineBWpoints();
        }
    }

	void DrawLineBWpoints()
    {
		line = GetComponent<LineRenderer>();
		//setting Sag Amplitude

		decimal argument = (decimal)startTransform.transform.localScale.y;
		int count = BitConverter.GetBytes(decimal.GetBits(argument)[3])[2];

		//Added Line for convinience check the decimal point and add the multiplier
		sagAmplitude_div_factor = Vector3.Distance(startTransform.transform.position, endPointTransform.position) * 3000*3;

		sagAmplitude = ((float)line.positionCount) / sagAmplitude_div_factor;

		if (!endPointTransform)
		{
			Debug.LogError("No Endpoint Transform assigned to Cable_Procedural component attached to " + gameObject.name);
			return;
		}

		// Get direction Vector.
		vectorFromStartToEnd = endPointTransform.position - startTransform.transform.position;
		// Setting the Start object to look at the end will be used for making the wind be perpendicular to the cable later.
		startTransform.transform.forward = vectorFromStartToEnd.normalized;
		// Get number of points in the cable using the distance from the start to end, and the point density
		pointsInLineRenderer = Mathf.FloorToInt(pointDensity * vectorFromStartToEnd.magnitude);
		// Set number of points in line renderer
		line.positionCount = pointsInLineRenderer;

		// The Direction of SAG is the direction of gravity
		sagDirection = Physics.gravity.normalized;

		Draw();
	}

    void Draw()
	{
		// What point is being calculated
		int i = 0;

		while(i < pointsInLineRenderer)
		{
			// This is the fraction of where we are in the cable and it accounts for arrays starting at zero.
			float pointForCalcs = (float)i / (pointsInLineRenderer - 1);
			// This is what gives the cable a curve and makes the wind move the center the most.
			float effectAtPointMultiplier = Mathf.Sin(pointForCalcs * Mathf.PI);

			// Calculate the position of the current point i
			Vector3 pointPosition = vectorFromStartToEnd * pointForCalcs;
			// Calculate the sag vector for the current point i
			Vector3 sagAtPoint = sagDirection * sagAmplitude;
			// Calculate the sway vector for the current point i

			// Calculate the postion with Sag.
			Vector3 currentPointsPosition =
				startTransform.transform.position + 
				pointPosition + 
				(Vector3.ClampMagnitude(sagAtPoint, sagAmplitude)) * effectAtPointMultiplier;

			// Set point
			line.SetPosition(i, currentPointsPosition);

			//**************************************Set the width********************************************//

			//line.SetWidth(transform.localPosition.y, transform.localScale.y);
			//MeshFilter mf = GetComponent<MeshFilter>();
			//if (mf != null)
			//{
			//Bounds bound = mf.mesh.bounds;


                //line.startWidth = bound.size.x/100;// *0.004f;// 0.0001f *20f;// bound.size.x* 0.00001f;
                //line.endWidth = line.startWidth;
                line.startWidth = startTransform.transform.localScale.y * 20;
                line.endWidth = line.startWidth;


            //}
            i++;
		}
	}
}
