using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class overlapSphere : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        ExplosionDamage(transform.position, 0.1f);
        
    }

    void ExplosionDamage(Vector3 center, float radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(center, radius);
        foreach (var hitCollider in hitColliders)
        {
            //hitCollider.SendMessage("AddDamage");
            if(hitCollider.name!=name)
            print(hitCollider.name);
        }
    }
}
