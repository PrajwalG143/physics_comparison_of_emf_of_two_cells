using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// This complete script can be attached to a camera to make it
// continuously point at another object.

public class LookAt : MonoBehaviour
{
    public Transform target;

    private void Start()
    { 
            if (GameObject.Find("AR Camera"))
            {
                target = GameObject.Find("AR Camera").transform;
            }
    }
    void Update()
    {
        // Rotate the camera every frame so it keeps looking at the target
        Vector3 targetPosition = new Vector3(target.transform.position.x, 
                                                    transform.position.y, 
                                             target.transform.position.z);

        // Same as above, but setting the worldUp parameter to Vector3.left in this example turns the camera on its side
        transform.LookAt(target);
    }
}