using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResistanceManager : MonoBehaviour
{
    Resistances r;
    public int value;
    public bool key1, key2;
    [SerializeField] private List<int> resistances;
    // Start is called before the first frame update
    void Start()
    {
        value = 0;
        for(int i = 0; i < resistances.Count; i++)
        {
            value += resistances[i];
        }
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(value);
    }

    public void addResistance(int r)
    {
        resistances.Add(r);
        for (int i = 0; i < resistances.Count; i++)
        {
            value += resistances[i];
        }
    }
}
