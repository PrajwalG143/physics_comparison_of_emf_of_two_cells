using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class FingerController : MonoBehaviour
{
    public GameObject RHand;
    public Transform[] FingerPositions = new Transform[2];
    Finger[] Rfingers = new Finger[5];
    bool coroutineCalled = false;
    // Start is called before the first frame update
    void Start()
    {
       Rfingers = RHand.GetComponent<FingerRig>().fingers;
    }

    // Update is called once per frame
    void Update()
    {
/*        if (Input.GetKeyDown(KeyCode.D) & !coroutineCalled)
        {
            StartCoroutine(CloseFingers());
            Debug.Log("D is Pressed");
        }
*/
    }

    IEnumerator CloseFingers()
    {
        coroutineCalled = true;
        while(Vector3.Distance(Rfingers[0].target.transform.position,FingerPositions[0].transform.position) > 0.001f)
        {
            Debug.Log("Closing fingers");
            Rfingers[0].target.transform.position = Vector3.Lerp(Rfingers[0].target.transform.position, FingerPositions[0].transform.position , Time.deltaTime );
            Rfingers[1].target.transform.position = Vector3.Lerp(Rfingers[1].target.transform.position, FingerPositions[1].transform.position, Time.deltaTime );
        }
        coroutineCalled = false;
        yield return null;
    }

    public void OnButtonClick()
    {
        StartCoroutine(CloseFingers());
    }
    public void OnButtonRelease()
    {
        StopCoroutine(CloseFingers());
    }
}
