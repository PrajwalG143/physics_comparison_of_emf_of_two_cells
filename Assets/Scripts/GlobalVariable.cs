using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalVariable
{
    public static string directedFromScene;

    public static void Clicked3DViewer(string currentSceneName)
    {
        directedFromScene = currentSceneName;
    }
}
