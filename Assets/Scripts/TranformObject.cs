using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranformObject : MonoBehaviour
{
    public Vector3 localPosition;
    public Quaternion quaternion;
    public float x = 0f;
    public float y = 0f;
    public float z = -25f;

    // Start is called before the first frame update

    public void tranformObject()
    {
        this.gameObject.transform.localEulerAngles = new Vector3(x, y, z);
    }

    public void setLocation()
    {
        this.gameObject.transform.localPosition =  (localPosition);
        this.gameObject.transform.localEulerAngles = quaternion.eulerAngles;
    }
}
