using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideMenu : MonoBehaviour
{
    public Scrollbar scrollBar;

    public float scrollPosition = 0;
    public float[] positionOfScrollMenu;
    float distance;
    float lerpTime = 0.75f;

    [SerializeField]
    Vector2 regularScale, highlightScale;

    // Start is called before the first frame update
    void Awake()
    {
        positionOfScrollMenu = new float[transform.childCount];
        distance = 1f / (positionOfScrollMenu.Length - 1);

        for (int i = 0; i < positionOfScrollMenu.Length; i++)
        {
            positionOfScrollMenu[i] = distance * i;
        }

        scrollBar = scrollBar.GetComponent<Scrollbar>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            scrollPosition = scrollBar.value;
            if (scrollPosition < 0)
            {
                scrollPosition = 0;
            }
            else if (scrollPosition > 1)
            {
                scrollPosition = 1;
            }
        }
        else
        {
            for (int i = 0; i < positionOfScrollMenu.Length; i++)
            {
                if (scrollPosition < positionOfScrollMenu[i] + (distance / 2) && scrollPosition > positionOfScrollMenu[i] - (distance / 2))
                {
                    scrollBar.value = Mathf.Lerp(scrollBar.value, positionOfScrollMenu[i], lerpTime);
                }
            }
        }

        for (int i = 0; i < positionOfScrollMenu.Length; i++)
        {
            if (scrollPosition < positionOfScrollMenu[i] + (distance / 2) && scrollPosition > positionOfScrollMenu[i] - (distance / 2))
            {
                transform.GetChild(i).localScale = Vector2.Lerp(transform.GetChild(i).localScale, highlightScale, lerpTime);
                /*
                for (int j = 0; j < positionOfScrollMenu.Length; j++)
                {
                    if (j != i)
                    {
                        transform.GetChild(j).localScale = Vector2.Lerp(transform.GetChild(j).localScale, regularScale, 0.1f);
                    }
                }
                */
            }
            else if (scrollPosition >= 0 && scrollPosition <= 1)
            {
                transform.GetChild(i).localScale = Vector2.Lerp(transform.GetChild(i).localScale, regularScale, lerpTime);
            }
        }
    }
}
