using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Lean;

public class ExperimentManager : MonoBehaviour
{
    public GameObject onionPeel;
    public GameObject slidePeel;
    public GameObject forcepTrf;
    public GameObject GlassSlide, GlassSlide1;
    public GameObject handL;
    public GameObject microscopeView;
    public ChangeText changeText;
    public changeShaderRed razorblade, glassSlide,microscope;
    public TextMeshProUGUI text;


    // Start is called before the first frame update
    private void Start()
    {
        text.text = "Take a vertical portion of onion bulb.";
    }
    public void OnionPeel()
    {
        onionPeel.SetActive(true);
        Debug.Log("OnionPeelRemoved");
    }

    public void Forceps()
    {
        StartCoroutine(LateCall());
        onionPeel.SetActive(false);
        slidePeel.SetActive(true);
        handL.SetActive(false);
        razorblade.HighLight();
        glassSlide.MakeNormal();
    }

    public void MicroView()
    {
        Debug.Log("microscope");
        GlassSlide.SetActive(false);
        GlassSlide1.SetActive(true);
        microscopeView.SetActive(true);
        changeText.changeText("Rotate the mirror to get the maximum light.");
        microscope.HighLight();
    }
    IEnumerator LateCall()
    {
        yield return new WaitForSeconds(3);
        forcepTrf.SetActive(false);
    }
}

public enum ObjectsCounts
{
    Forceps,
    OnionPeel,
    GlassSlide,
    GlassSlide1,
    GlassCover,
    Microscope,
    Burner,
    RazorBlade,
    BloattingPaper,
    Needle
}
