using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionOptionButtonChange : MonoBehaviour
{
    [SerializeField]
    Sprite defaultSprite;
    [SerializeField]
    Sprite clickedSprite;

    [SerializeField]
    Sprite correctSprite;
    [SerializeField]
    Sprite wrongSprite;

    public void ChangeSprite(bool changeSprite)
    {
        if (changeSprite)
            transform.GetComponent<Image>().sprite = clickedSprite;
        else
            transform.GetComponent<Image>().sprite = defaultSprite;
    }

    public void ChangeSpriteOnShowingSolution(bool changeSprite)
    {
        if(changeSprite)
            transform.GetComponent<Image>().sprite = correctSprite;
        else
            transform.GetComponent<Image>().sprite = wrongSprite;
    }
}
