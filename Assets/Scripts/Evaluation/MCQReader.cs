using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using UnityEngine.Networking;
using System.Text;
using System.Web;

public class MCQReader : MonoBehaviour
{
    public List<QAClass> QuestionAnswers;

    [Header("XML Question File URL")]
    public string url;
    public GameObject GameObjectContainingMCQHandler;

    // Start is called before the first frame update
    void Awake()
    {
        StartCoroutine(FeedQuestions());
    }

    IEnumerator FeedQuestions()
    {
        var xml = new XmlDocument();

        // Start a download of the given URL
        Debug.Log("...from " + url);
        UnityWebRequest www = UnityWebRequest.Get(url);
        Debug.Log("yielding");

        // Wait for download to complete
        yield return www.SendWebRequest();
        // Debug.Log(www.downloadedBytes);
        Debug.Log("yielded");



        xml.LoadXml(www.downloadHandler.text);

        var wrapperNode = xml.LastChild;
        if (wrapperNode.Name != "questionlist")
        {
            // Debug.LogError("This is not a wrapper file");
        }
        Debug.Log("got data");

        var cnodeCount = wrapperNode.ChildNodes.Count;
        // Debug.Log("got " + cnodeCount);
        QuestionAnswers = new List<QAClass>();
        for (int i = 0; i < cnodeCount; i++)
        {
            QAClass qa = new QAClass();

            // Debug.Log("got " + cnodeCount);
            var dataNode = wrapperNode.ChildNodes.Item(i);
            var cCount = dataNode.ChildNodes.Count;
            var variable = "";

            for (var j = 0; j < cCount; j++)
            {
                if (dataNode.ChildNodes.Item(j).Name == "A")
                {
                    variable = dataNode.ChildNodes.Item(j).InnerText;
                    // Debug.Log(variable);
                    qa.OptionA = variable.ToString();
                }
                else if (dataNode.ChildNodes.Item(j).Name == "B")
                {
                    variable = dataNode.ChildNodes.Item(j).InnerText;
                    // Debug.Log(variable);
                    qa.OptionB = variable.ToString();
                }
                else if (dataNode.ChildNodes.Item(j).Name == "C")
                {
                    variable = dataNode.ChildNodes.Item(j).InnerText;
                    // Debug.Log(variable);
                    qa.OptionC = variable.ToString();
                }
                else if (dataNode.ChildNodes.Item(j).Name == "D")
                {
                    variable = dataNode.ChildNodes.Item(j).InnerText;
                    // Debug.Log(variable);
                    qa.OptionD = variable.ToString();
                }
                else if (dataNode.ChildNodes.Item(j).Name == "ans")
                {
                    variable = dataNode.ChildNodes.Item(j).InnerText;
                    // Debug.Log(variable);
                    qa.AnsKey = variable.ToString();
                }
                else if (dataNode.ChildNodes.Item(j).Name == "questiontext")
                {
                    variable = dataNode.ChildNodes.Item(j).InnerText;
                    // Debug.Log(variable);
                    qa.Question = variable.ToString();
                }
                
                // Debug.Log(dataNode.Attributes.GetNamedItem("number").Value);
                qa.QNo = int.Parse(dataNode.Attributes.GetNamedItem("number").Value);
            }

            qa.QNo = i+1;
            QuestionAnswers.Add(qa);
        }

        GameObjectContainingMCQHandler.GetComponent<MCQHandler>().SetUpQuestionList(QuestionAnswers);
        // Debug.Log(QuestionAnswers);
    }
}

