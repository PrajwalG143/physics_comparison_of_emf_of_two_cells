﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MCQHandler : MonoBehaviour
{
    [Header("Header Panel")]
    public TMP_Text statusText;
    public TMP_Text timerText;

    [Header("QA Panel")]
    public GameObject qaPanel;
    public TMP_Text questionText;
    public GameObject[] optionsButtonList;
    public TMP_Text[] optionsText;
    public GameObject previousButton;
    public GameObject nextButton;

    [Header("QuestionList Panel")]
    public GameObject questionButtonHolder;
    public GameObject questionButton; 
    public GameObject[] questionButtonsList;
    public TMP_Text unansweredQLCountText;

    [Header("TimeOut Panel")]
    public GameObject timeOutPanel;

    [Header("Exit Panel")]
    public GameObject exitPanel;

    [Header("Marksheet Panel")]
    public GameObject marksheetPanel;
    public TMP_Text correctAnswersCountText;
    public TMP_Text wrongAnswersCountText;
    public TMP_Text unAnsweredCountText;
    public TMP_Text totalText;

    [Header("Misc")]
    public GameObject[] questionPanelHideList;

    public int currentQuestion = 1;
    public int currentOption;

    public int correctAnswersCount;
    public int wrongAnswersCount;
    public int unAnsweredCount;

    [Header("Timer")]
    public float timeLimit = 300;
    float timeRemaining;
    bool timerRunning;

    bool showSolution;

    public List<QAClass> qaList;
    public List<QAStatus> qaStatusList;

    private void Awake()
    {
        currentQuestion = 1;

        for(int i = 0; i < optionsButtonList.Length; i++)
        {
            int parameter = i + 1;
            optionsButtonList[i].GetComponent<Button>().onClick.AddListener(() => OnOptionsButtonClicked(parameter));
        }

        timeRemaining = timeLimit;
        timerRunning = true;
    }

    private void Update()
    {
        // Timer
        if (timerRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                // Timer Completed
                timeRemaining = 0;
                timerRunning = false;

                timeOutPanel.SetActive(true);
                foreach (GameObject child in questionPanelHideList)
                    child.SetActive(false);
                exitPanel.SetActive(false);
            }

            // Timer Display
            timerText.text = string.Format("{0:00}:{1:00}", Mathf.FloorToInt(timeRemaining / 60), Mathf.FloorToInt(timeRemaining % 60));
        }
    }

    public void OnNextOrPreviousButtonClicked(int changeValue)
    {
        currentQuestion = currentQuestion + changeValue;

        if (currentQuestion == 1)
            previousButton.GetComponent<Button>().interactable = false;
        else
            previousButton.GetComponent<Button>().interactable = true;

        if (currentQuestion == qaList.Count)
            nextButton.GetComponent<Button>().interactable = false;
        else
            nextButton.GetComponent<Button>().interactable = true;

        LoadQuestion();
    }

    public void OnQuestionSelectButtonClicked(int questionNumber)
    {
        currentQuestion = questionNumber;
        LoadQuestion();

        if (currentQuestion == 1)
            previousButton.GetComponent<Button>().interactable = false;
        else
            previousButton.GetComponent<Button>().interactable = true;

        if (currentQuestion == qaList.Count)
            nextButton.GetComponent<Button>().interactable = false;
        else
            nextButton.GetComponent<Button>().interactable = true;

        // Debug.Log(questionNumber);
    }

    public void OnOptionsButtonClicked(int option)
    {
        currentOption = option;
        for (int i = 0; i < optionsButtonList.Length; i++)
        {
            optionsButtonList[i].GetComponent<QuestionOptionButtonChange>().ChangeSprite(i == (option - 1));
        }

        // Debug.Log(option);
        UpdateQuestion();
    }

    public void LoadQuestion()
    {
        questionText.text = "Q " + qaList[currentQuestion - 1].QNo.ToString("00") + ". <indent=10%>" + qaList[currentQuestion - 1].Question;
        optionsText[0].text = qaList[currentQuestion - 1].OptionA;
        optionsText[1].text = qaList[currentQuestion - 1].OptionB;
        optionsText[2].text = qaList[currentQuestion - 1].OptionC;
        optionsText[3].text = qaList[currentQuestion - 1].OptionD;

        if(!showSolution)
        {
            for (int i = 0; i < questionButtonsList.Length; i++)
            {
                questionButtonsList[i].GetComponent<QuestionSelectButtonChange>().ChangeSprite(qaStatusList[i].isAnswered, i == (currentQuestion - 1));
            }

            for (int i = 0; i < optionsButtonList.Length; i++)
            {
                optionsButtonList[i].GetComponent<QuestionOptionButtonChange>().ChangeSprite(i == (qaStatusList[currentQuestion - 1].SelectedOption - 1));
            }

            currentOption = qaStatusList[currentQuestion - 1].SelectedOption;
        }
        else
        {
            for (int i = 0; i < questionButtonsList.Length; i++)
            {
                questionButtonsList[i].GetComponent<QuestionSelectButtonChange>().ChangeSpriteOnShowingSolution(qaStatusList[i].isAnswered, qaStatusList[i].isCorrect, i == (currentQuestion - 1));
            }

            for (int i = 0; i < optionsButtonList.Length; i++)
            {
                if (i == (qaStatusList[currentQuestion - 1].CorrectOption - 1))
                    optionsButtonList[i].GetComponent<QuestionOptionButtonChange>().ChangeSpriteOnShowingSolution(true);
                else if ((i == (qaStatusList[currentQuestion - 1].SelectedOption - 1)) && qaStatusList[currentQuestion - 1].isAnswered && !qaStatusList[currentQuestion - 1].isCorrect)
                    optionsButtonList[i].GetComponent<QuestionOptionButtonChange>().ChangeSpriteOnShowingSolution(false);
                else
                    optionsButtonList[i].GetComponent<QuestionOptionButtonChange>().ChangeSprite(false);
            }
        }
        
    }

    public void UpdateQuestion()
    {
        if (currentOption != 0)
        {
            qaStatusList[currentQuestion - 1].SelectedOption = currentOption;

            switch (currentOption)
            {
                case 1:
                    qaStatusList[currentQuestion - 1].SelectedAns = qaList[currentQuestion - 1].OptionA;
                    break;
                case 2:
                    qaStatusList[currentQuestion - 1].SelectedAns = qaList[currentQuestion - 1].OptionB;
                    break;
                case 3:
                    qaStatusList[currentQuestion - 1].SelectedAns = qaList[currentQuestion - 1].OptionC;
                    break;
                case 4:
                    qaStatusList[currentQuestion - 1].SelectedAns = qaList[currentQuestion - 1].OptionD;
                    break;
            }

            qaStatusList[currentQuestion - 1].isAnswered = true;

            switch (qaList[currentQuestion - 1].AnsKey)
            {
                case "A":
                    qaStatusList[currentQuestion - 1].isCorrect = (qaStatusList[currentQuestion - 1].SelectedAns == qaList[currentQuestion - 1].OptionA);
                    break;
                case "B":
                    qaStatusList[currentQuestion - 1].isCorrect = (qaStatusList[currentQuestion - 1].SelectedAns == qaList[currentQuestion - 1].OptionB);
                    break;
                case "C":
                    qaStatusList[currentQuestion - 1].isCorrect = (qaStatusList[currentQuestion - 1].SelectedAns == qaList[currentQuestion - 1].OptionC);
                    break;
                case "D":
                    qaStatusList[currentQuestion - 1].isCorrect = (qaStatusList[currentQuestion - 1].SelectedAns == qaList[currentQuestion - 1].OptionD);
                    break;
            }

            questionButtonsList[currentQuestion - 1].GetComponent<QuestionSelectButtonChange>().ChangeSprite(qaStatusList[currentQuestion - 1].isAnswered, true);

            // Updating unanswered questions count
            unAnsweredCount = qaStatusList.Count;
            for (int i = 0; i < qaStatusList.Count; i++)
            {
                if (qaStatusList[i].isAnswered)
                    unAnsweredCount -= 1;
            }
            unansweredQLCountText.text = unAnsweredCount.ToString("00");
        }
    }

    public void OnFinishQuizButtonClicked(bool finishQuiz)
    {
        foreach (GameObject child in questionPanelHideList)
            child.SetActive(!finishQuiz);

        exitPanel.SetActive(finishQuiz);
    }

    public void OnMarksheetButtonClicked()
    {
        timerRunning = false;

        exitPanel.SetActive(false);
        marksheetPanel.SetActive(true);

        correctAnswersCount = 0;
        wrongAnswersCount = 0;
        unAnsweredCount = 0;

        for (int i = 0; i < qaStatusList.Count; i++)
        {
            if (qaStatusList[i].isAnswered)
            {
                if (qaStatusList[i].isCorrect)
                    correctAnswersCount += 1;
                else
                    wrongAnswersCount += 1;
            }
            else
            {
                unAnsweredCount += 1;
            }
        }

        correctAnswersCountText.text = correctAnswersCount.ToString("00");
        wrongAnswersCountText.text = wrongAnswersCount.ToString("00");
        unAnsweredCountText.text = unAnsweredCount.ToString("00");

        totalText.text = correctAnswersCountText.text + "/" + qaList.Count.ToString("00");
    }

    public void OnRetakeQuizButtonClicked()
    {
        timerRunning = true;
        timeRemaining = timeLimit;

        marksheetPanel.SetActive(false);
        OnFinishQuizButtonClicked(false);

        currentQuestion = 1;
        currentOption = 0;

        for (int i = 0; i < qaList.Count; i++)
        {
            qaStatusList[i].SelectedOption = 0;
            qaStatusList[i].SelectedAns = "";
            qaStatusList[i].isAnswered = false;
            qaStatusList[i].isCorrect = false;
        }

        unAnsweredCount = qaStatusList.Count;
        unansweredQLCountText.text = unAnsweredCount.ToString("00");

        if (currentQuestion == 1)
            previousButton.GetComponent<Button>().interactable = false;
        else if (currentQuestion == qaList.Count)
            nextButton.GetComponent<Button>().interactable = false;

        LoadQuestion();
    }

    public void OnSolutionsButtonClicked()
    {
        statusText.text = "Solutions";
        marksheetPanel.SetActive(false);
        foreach (GameObject child in questionPanelHideList)
        {
            if(child.name == "QA Panel" || child.name == "QuestionSelect Holder") 
            {
                child.SetActive(true);
            }
            else
            {
                child.SetActive(false);
            }
        }

        for (int i = 0; i < qaList.Count; i++)
        {
            switch (qaList[i].AnsKey)
            {
                case "A":
                    qaStatusList[i].CorrectOption = 1;
                    break;
                case "B":
                    qaStatusList[i].CorrectOption = 2;
                    break;
                case "C":
                    qaStatusList[i].CorrectOption = 3;
                    break;
                case "D":
                    qaStatusList[i].CorrectOption = 4;
                    break;
            }
            qaStatusList[i].CorrectAns = "";
        }
        showSolution = true;

        currentQuestion = 1;      

        for (int i = 0; i < optionsButtonList.Length; i++)
        {
            optionsButtonList[i].GetComponent<Button>().enabled = false;
        }

        if (currentQuestion == 1)
        {
            previousButton.GetComponent<Button>().interactable = false;
            nextButton.GetComponent<Button>().interactable = true;
        }
        else if (currentQuestion == qaList.Count)
        {
            previousButton.GetComponent<Button>().interactable = true;
            nextButton.GetComponent<Button>().interactable = false;
        }
        LoadQuestion();
    }

    // Recieves the list of questions and initiates the questions.
    public void SetUpQuestionList(List<QAClass> qaClassList)
    {
        qaList = qaClassList;

        if (qaList != null && qaList.Count > 0)
        {
            qaStatusList = new List<QAStatus>();
            questionButtonsList = new GameObject[qaList.Count];

            currentQuestion = 1;
            statusText.text = "Loading....";

            for(int i = 0; i < qaList.Count; i++)
            {
                QAStatus qaStatus = new QAStatus();

                questionButtonsList[i] = Instantiate(questionButton, questionButtonHolder.transform);
                questionButtonsList[i].GetComponentInChildren<TextMeshProUGUI>().text = (i + 1).ToString();
                questionButtonsList[i].gameObject.name = "Question " + (i + 1).ToString();
                questionButtonsList[i].gameObject.SetActive(true);
                int parameter = i + 1;
                questionButtonsList[i].GetComponent<Button>().onClick.AddListener(() => OnQuestionSelectButtonClicked(parameter));

                qaStatus.QNo = parameter;
                qaStatus.SelectedOption = 0;
                qaStatus.SelectedAns = "";
                qaStatus.isAnswered = false;
                qaStatus.isCorrect = false;
                qaStatusList.Add(qaStatus);
            }

            LoadQuestion();
        }

        statusText.text = "";
        unAnsweredCount = qaStatusList.Count;
        unansweredQLCountText.text = unAnsweredCount.ToString("00");

        if (currentQuestion == 1)
            previousButton.GetComponent<Button>().interactable = false;
        else if (currentQuestion == qaList.Count)
            nextButton.GetComponent<Button>().interactable = false;
    }
}
