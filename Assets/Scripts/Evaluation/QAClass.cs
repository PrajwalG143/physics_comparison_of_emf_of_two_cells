﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QAClass
{
    public int QNo { get; set; }

    public string Question { get; set; }

    public string OptionA { get; set; }

    public string OptionB { get; set; }

    public string OptionC { get; set; }

    public string OptionD { get; set; }

    public string AnsKey { get; set; }
}

public class QAStatus
{
    public int QNo { get; set; }

    public int SelectedOption { get; set; }

    public string SelectedAns { get; set; }

    public int CorrectOption { get; set; }

    public string CorrectAns { get; set; }

    public bool isAnswered { get; set; }

    public bool isCorrect { get; set; }
}