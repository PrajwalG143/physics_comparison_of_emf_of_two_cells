using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionSelectButtonChange : MonoBehaviour
{
    [SerializeField]
    Sprite defaultUnansweredSprite;
    [SerializeField]
    Sprite clickedUnansweredSprite;
    [SerializeField]
    Sprite defaultAnsweredSprite;
    [SerializeField]
    Sprite clickedAnsweredSprite;

    [SerializeField]
    Sprite defaultCorrectSprite;
    [SerializeField]
    Sprite clickedCorrectSprite;
    [SerializeField]
    Sprite defaultWrongSprite;
    [SerializeField]
    Sprite clickedWrongSprite;

    public void ChangeSprite(bool isAnswered, bool isSelected)
    {
        if (isAnswered)
        {
            if (isSelected)
                transform.GetComponent<Image>().sprite = clickedAnsweredSprite;
            else
                transform.GetComponent<Image>().sprite = defaultAnsweredSprite;
        }
        else
        {
            if (isSelected)
                transform.GetComponent<Image>().sprite = clickedUnansweredSprite;
            else
                transform.GetComponent<Image>().sprite = defaultUnansweredSprite;
        }
    }

    public void ChangeSpriteOnShowingSolution(bool isAnswered, bool isCorrect, bool isSelected)
    {
        if (isAnswered)
        {
            if (isCorrect)
            {
                if (isSelected)
                    transform.GetComponent<Image>().sprite = clickedCorrectSprite;
                else
                    transform.GetComponent<Image>().sprite = defaultCorrectSprite;
            }
            else
            {
                if (isSelected)
                    transform.GetComponent<Image>().sprite = clickedWrongSprite;
                else
                    transform.GetComponent<Image>().sprite = defaultWrongSprite;
            }  
        }
        else
        {
            if (isSelected)
                transform.GetComponent<Image>().sprite = clickedUnansweredSprite;
            else
                transform.GetComponent<Image>().sprite = defaultUnansweredSprite;
        }
    }
}
