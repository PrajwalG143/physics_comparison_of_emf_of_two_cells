using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationController : MonoBehaviour
{
    // Start is called before the first frame update

    public Animator animator;
    public string animName , animName1;
    public bool toggleState;

    private void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }
    public void onToggled()
    {
        if (toggleState)
        {
            animator.Play(animName);
            toggleState = false;
        }
        else
        {
            animator.Play(animName1);
            toggleState = true;
        }
    }
}
