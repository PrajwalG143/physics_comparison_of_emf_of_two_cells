using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScreenKeyboardAdjusterForInputField : MonoBehaviour
{
    // Assign panel here in order to adjust its height when TouchScreenKeyboard is shown
    public GameObject panel;

    private TMP_InputField inputField;
    private RectTransform panelRectTransform;
    private Vector2 panelOffsetMinOriginal, panelOffsetMaxOriginal;
    private float panelHeightOriginal;
    private float currentKeyboardHeightRatio;

    float inputFieldHeightRatio;

    bool keepOldTextInInputField = false;
    string oldEditText, editText;

    bool moveToEndOfTheLine = false;

   
    public void OnEnable()
    {
        editText = transform.GetComponent<TMP_InputField>().text;
        oldEditText = transform.GetComponent<TMP_InputField>().text;
    }
    
    public void Start()
    {
        inputField = transform.GetComponent<TMP_InputField>();
        panelRectTransform = panel.GetComponent<RectTransform>();
        panelOffsetMinOriginal = panelRectTransform.offsetMin;
        panelOffsetMaxOriginal = panelRectTransform.offsetMax;
        panelHeightOriginal = panelRectTransform.rect.height;

        inputFieldHeightRatio = (transform.GetComponent<RectTransform>().position.y - (10.0f * transform.GetComponent<RectTransform>().rect.height)) / Screen.safeArea.height;

        // Retain the text when back button is pressed on Android
        inputField.onValueChanged.AddListener(EditingInputField);
        inputField.onEndEdit.AddListener(EndEditOfInputField);
        inputField.onTouchScreenKeyboardStatusChanged.AddListener(ReportStatusChangeOfTouchScreenKeyboard);

        Debug.Log("Start Call");
    }

    public void LateUpdate()
    {
        if (inputField.isFocused)
        {            
            float newKeyboardHeightRatio = GetKeyboardHeightRatio();
            if (currentKeyboardHeightRatio != newKeyboardHeightRatio)
            {
                Debug.Log("InputFieldForScreenKeyboardPanelAdjuster: Adjust to keyboard height ratio: " + newKeyboardHeightRatio);
                currentKeyboardHeightRatio = newKeyboardHeightRatio;

                if (inputFieldHeightRatio < currentKeyboardHeightRatio)
                {
                    panelRectTransform.offsetMin = new Vector2(panelOffsetMinOriginal.x, panelOffsetMinOriginal.y + panelHeightOriginal * Mathf.Abs(currentKeyboardHeightRatio - inputFieldHeightRatio));
                    panelRectTransform.offsetMax = new Vector2(panelOffsetMaxOriginal.x, panelOffsetMaxOriginal.y + panelHeightOriginal * Mathf.Abs(currentKeyboardHeightRatio - inputFieldHeightRatio));
                }
            }
        }
        else if (currentKeyboardHeightRatio != 0f)
        {
            if (panelRectTransform.offsetMin != panelOffsetMinOriginal)
            {
                Invoke("DelayedReset", 0);
            }
            currentKeyboardHeightRatio = 0f;
        }
       
        if(moveToEndOfTheLine)
        {
            inputField.MoveToEndOfLine(false, true);
            moveToEndOfTheLine = !moveToEndOfTheLine;
        }
    }

    void DelayedReset()
    {
        Debug.Log("InputFieldForScreenKeyboardPanelAdjuster: Revert to original");
        panelRectTransform.offsetMin = panelOffsetMinOriginal;
        panelRectTransform.offsetMax = panelOffsetMaxOriginal;
    }

    /// <summary>
    /// The function returns the height if the keyboard in runtime
    /// </summary>
    /// <returns></returns>
    private float GetKeyboardHeightRatio()
    {
        if (Application.isEditor)
        {
            // To create fake TouchScreenKeyboard height ratio for debug in editor 
            return 0.4f;        
        }

#if UNITY_ANDROID        
        using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");
            using (AndroidJavaObject rect = new AndroidJavaObject("android.graphics.Rect"))
            {
                View.Call("getWindowVisibleDisplayFrame", rect);
                return (float)(Screen.safeArea.height - rect.Call<int>("height")) / Screen.safeArea.height;
            }
        }
#else
        return (float)TouchScreenKeyboard.area.height / Screen.safeArea.height;
#endif
    }

    private void EditingInputField(string currentText)
    {
        oldEditText = editText;
        editText = currentText;
    }

    private void EndEditOfInputField(string currentText)
    {
        if(keepOldTextInInputField)
        {
            // On EndEdit the text in InputField becomes empty ("Text in InputField" -> "" => oldEditText -> editText)
            // So we assign oldEditText to InputField 
            editText = oldEditText;
            inputField.text = oldEditText;
            moveToEndOfTheLine = true;

            keepOldTextInInputField = false;
        }
    }

    private void ReportStatusChangeOfTouchScreenKeyboard(TouchScreenKeyboard.Status newStatus)
    {
        if (newStatus == TouchScreenKeyboard.Status.Canceled)
        {
            keepOldTextInInputField = true;
        }
    }
}
