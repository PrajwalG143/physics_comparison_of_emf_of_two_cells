
using UnityEngine;
/// <summary>
/// This script is needed for PC URP builds Green Color fix in starting.
/// color is not serialized in the UnitySimpleLiquid Package
/// </summary>
public class InitializeLiquidColor : MonoBehaviour
{
    public Color initialColor;
    private void OnEnable()
    {
        GetComponent<UnitySimpleLiquid.LiquidContainer>().LiquidColor = initialColor;
    }
}