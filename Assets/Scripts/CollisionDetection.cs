using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    public ObjectsCounts experimentManager;
    public ExperimentManager experiment;

    private void Start()
    {
        experiment = FindObjectOfType<ExperimentManager>();
    }

    public void OnCollisionEnter(Collision collision)
    {
     //   Debug.Log("Collied"+collision.gameObject.name);

        if(experimentManager == ObjectsCounts.Forceps && collision.gameObject.GetComponent<CollisionDetection>().experimentManager == ObjectsCounts.OnionPeel)
        {
            experiment.OnionPeel();
        }

        if (experimentManager == ObjectsCounts.Forceps && collision.gameObject.GetComponent<CollisionDetection>().experimentManager == ObjectsCounts.GlassSlide)
        {
            experiment.Forceps();
            
        }

        if (experimentManager == ObjectsCounts.GlassSlide1 && collision.gameObject.GetComponent<CollisionDetection>().experimentManager == ObjectsCounts.Microscope)
        {
            experiment.MicroView();
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        
    }

}

  