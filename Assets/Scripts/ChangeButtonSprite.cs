using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeButtonSprite : MonoBehaviour
{
    [SerializeField]
    Sprite defaultSprite;
    [SerializeField]
    Sprite onClickedSprite;

    bool changeSprite = false;

    private void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(OnButtonClicked);
    }

    public void OnButtonClicked()
    {
        changeSprite = !changeSprite;

        if(changeSprite)
        {
            this.GetComponent<Image>().sprite = onClickedSprite;
        }
        else
        {
            this.GetComponent<Image>().sprite = defaultSprite;
        }
    }

    public void OnOtherButtonClicked()
    {
        if(changeSprite)
        {
            OnButtonClicked();
        }
    }
}
