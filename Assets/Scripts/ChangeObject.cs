using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeObject : MonoBehaviour
{
    // Scene Type
    [SerializeField]
    UIAndSceneManager sceneTypeObject;

    // sceneType = 0 => Screen Space
    // sceneType = 1 => World Space
    public Button[] previousButton;
    public Button[] nextButton;
    public Slider[] progressBar;

    [SerializeField]
    SlideMenu[] slideMenuObject;

    private int currentGameobject;

    private void Awake()
    {
        if (nextButton == null)
        {
            nextButton[0] = GetComponent<Button>();
            nextButton[1] = GetComponent<Button>();
        }

        slideMenuObject[0] = slideMenuObject[0].GetComponent<SlideMenu>();
        slideMenuObject[1] = slideMenuObject[1].GetComponent<SlideMenu>();

        sceneTypeObject = sceneTypeObject.GetComponent<UIAndSceneManager>();

        SelectObject(0);
    }

    public void SelectObject(int _index)
    {
        currentGameobject = _index;
        previousButton[sceneTypeObject.sceneType].interactable = (_index != 0);
        nextButton[sceneTypeObject.sceneType].interactable = (_index != transform.childCount-1);

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(i == _index);
            if(i == _index)
            {
                if (slideMenuObject[sceneTypeObject.sceneType].positionOfScrollMenu.Length > 0)
                    slideMenuObject[sceneTypeObject.sceneType].scrollPosition = slideMenuObject[sceneTypeObject.sceneType].positionOfScrollMenu[_index];
            }
        }
    }

    public void ChangeObjects(int _change)
    {
        currentGameobject += _change;
        SelectObject(currentGameobject);
    }

    public void OnClickNext()
    {
        progressBar[sceneTypeObject.sceneType].value += 100/7f;
    }

    public void OnClickPrev()
    {
        progressBar[sceneTypeObject.sceneType].value -= 100/7f;
    }

    public void Invoke()
    {
        if (nextButton != null && nextButton[sceneTypeObject.sceneType].onClick != null)
            nextButton[sceneTypeObject.sceneType].onClick.Invoke();
    }

}
