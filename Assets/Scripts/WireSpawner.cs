using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireSpawner : MonoBehaviour
{
    [SerializeField] private GameObject connectingWire_pos_obj;
    [SerializeField] private GameObject wire_prefab;
    [SerializeField] private Transform parent_pins;
    public Transform plug1, plug2;
    bool p1, p2;

    private void Start()
    {
        if(parent_pins==null)
        {
            Transform par = transform.parent;
            if (par!=null)
            {
                parent_pins = par;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
     

        // **********To check if 2 pins were moved from their positions****************//

         if (plug1 != null && plug2 != null)
        {
            if (other.transform == plug1)
            {
                p1 = true;
            }
            else if (other.transform == plug2)
            {
                p2 = true;
            }

            if (p1 && p2)
            {
                //Transform new_wire = Instantiate(wire_prefab, connectingWire_pos_obj.transform.position, Quaternion.identity,parent_pins).transform;//new Vector3(-0.191f, 1.539f, 0.06f)
                Transform new_wire = Instantiate(wire_prefab, parent_pins,false).transform;
                new_wire.transform.parent = connectingWire_pos_obj.transform.parent;
                new_wire.transform.localPosition = connectingWire_pos_obj.transform.localPosition;

                p1 = false;
                p2 = false;

                //******************Taking reference of two pins of the new wire generated**************************//

                plug1 = new_wire.GetChild(0);
                plug2 = new_wire.GetChild(1);
            }
        }
    }
}
