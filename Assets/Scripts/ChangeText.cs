using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChangeText : MonoBehaviour
{

    public TextMeshProUGUI text;

    // Start is called before the first frame update
    void Start()
    {
        //text.text = "Take a vertical portion of onion bulb.";
        //StartCoroutine(printText());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void changeText(string textNote)
    {
        text.text = textNote;
    }

    IEnumerator printText()
    {
        yield return new WaitForSeconds(0.1f);
        text.text = "Drag the scaly leaf to separate it from the wedge.";
    }
}
