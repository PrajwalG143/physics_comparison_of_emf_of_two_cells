﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.SceneManagement;
using TMPro;
using System.Text.RegularExpressions;

public class UserData
{
    public string email;
    public string password;
}

public class LoginAPI : MonoBehaviour
{
    string hwurl =  "https://portal-api.holo-world.io/users/login";

    [SerializeField]
    TMP_InputField emailId;
    [SerializeField]
    TMP_InputField password;
    [SerializeField]
    TMP_Text returnMessage;

    [SerializeField]
    GameObject loading_Panel;
    [SerializeField]
    GameObject loadingCircle;

    public const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public static bool validateEmail(string email)
    {
        if (email != null)
            return Regex.IsMatch(email, MatchEmailPattern);
        else
            return false;
    }

    IEnumerator LoginCall(string username, string passwd)
    {
        var user = new UserData();
        user.email = username;
        user.password = passwd;

        string json = JsonUtility.ToJson(user);
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);

        //List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        //formData.Add(new MultipartFormFileSection("email", username));
        //formData.Add(new MultipartFormFileSection("password", passwd));

        //WWWForm wwwform = new WWWForm();
        //wwwform.AddField("email", username);
        //wwwform.AddField("password", passwd);

        //UnityWebRequest req = UnityWebRequest.Post(signUpurl, formData);


        UnityWebRequest req = new UnityWebRequest(hwurl, "POST");

        req.uploadHandler = new UploadHandlerRaw(jsonToSend);
        req.downloadHandler = new DownloadHandlerBuffer();
        req.SetRequestHeader("Content-Type", "application/json");
        
        //var jsonVariable = JsonUtility.ToJson(req.uploadHandler.data);
        //Debug.Log(JsonUtility.ToJson(req.uploadHandler.data));

        yield return req.SendWebRequest();

        if (req.isNetworkError)
        {
            Debug.Log("Error While Sending: " + req.error);
            returnMessage.text = "Network Error !";
        }
        else
        {
            if (req.isNetworkError)
            {
                Debug.Log("Error While Sending: " + req.error);
                returnMessage.text = "Network Error !";
            }
            else
            {
                Debug.Log("Received: " + req.downloadHandler.text);
                JSONNode resjson = JSON.Parse(req.downloadHandler.text);

                // string msg = resjson[0][0][0];
                string msg = "";
                foreach (KeyValuePair<string, JSONNode> keyValuePair in (JSONObject)resjson)
                {
                    if(keyValuePair.Key == "success")
                    {
                        msg = resjson[0]["msg"];
                        break;
                    }
                    else if(keyValuePair.Key == "errors")
                    {
                        msg = resjson[0][0]["msg"];
                        break;
                    }                   
                }               

                // Debug.Log(msg);

                if (msg == "email id not found !")
                {
                    Debug.Log("test : email id not found !");
                    loadingCircle.SetActive(false);
                    returnMessage.text = "Email id not found !";
                }
                else if (msg == "password is not matched!")
                {
                    Debug.Log("test : password is not matched!");
                    loadingCircle.SetActive(false);
                    returnMessage.text = "Password is not matched!";
                }
                else if (msg == "Account is not activated!")
                {
                    Debug.Log("test : Account is not activated!");
                    loadingCircle.SetActive(false);
                    returnMessage.text = "Account is not activated!";
                }
                else if (msg == "SignIn Successfully")
                {
                    Debug.Log("test : SignUp Successfully");
                    loadingCircle.SetActive(false);
                    returnMessage.text = "SignIn Successfully";

                    loading_Panel.SetActive(true);
                    Screen.orientation = ScreenOrientation.LandscapeLeft;
                    yield return new WaitForSeconds(1);

                    SceneManager.LoadScene(1);
                }
                else
                {
                    loadingCircle.SetActive(false);
                    returnMessage.text = resjson[0][1][1];
                }

            }
        }
    }

    public void OnLogInPressed()
    {
        if (validateEmail(emailId.text))
        {
            loadingCircle.SetActive(true);
            StartCoroutine(LoginCall(emailId.text, password.text));
        }
        else
        {
            returnMessage.text = "Enter a valid Email Id";
        }
    }
}
