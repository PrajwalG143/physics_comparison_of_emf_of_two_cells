﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;

public class SendOTPData
{
    public string email;
}
public class SendOTP : MonoBehaviour
{
    [SerializeField]
    TMP_InputField email;
    [SerializeField]
    TMP_Text errorMessage;
    [SerializeField]
    TMP_Text errorMessageResendOTP;

    [SerializeField]
    GameObject loadingCircle;

    public const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    string url = "https://portal-api.holo-world.io/users/sendOtp";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static bool validateEmail(string email)
    {
        if (email != null)
            return Regex.IsMatch(email, MatchEmailPattern);
        else
            return false;
    }

    // Send OTP Function Call
    IEnumerator callsendotp(string email)
    {
        var otpmail = new SendOTPData();
        otpmail.email = email;
      

        string json = JsonUtility.ToJson(otpmail);
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);

        UnityWebRequest req = new UnityWebRequest(url, "PATCH");

        req.uploadHandler = new UploadHandlerRaw(jsonToSend);
        req.downloadHandler = new DownloadHandlerBuffer();
        req.SetRequestHeader("Content-Type", "application/json");

        yield return req.SendWebRequest();

        if (req.isNetworkError)
        {
            loadingCircle.SetActive(false);
            errorMessage.text = "Error While Sending: " + req.error;
            Debug.Log("Error While Sending: " + req.error);
        }
        else
        {
            Debug.Log("Received: " + req.downloadHandler.text);
            JSONNode resjson = JSON.Parse(req.downloadHandler.text);

            string msg = resjson[0][0]["msg"];
            Debug.Log(msg);

            if (resjson[0][0]["msg"] == "OTP Send Unsuccessfully")
            {
                loadingCircle.SetActive(false);
                errorMessage.text = resjson[0][0]["msg"];
                Debug.Log("test : OTP Send Unsuccessfully");
            }
            else if (resjson[0]["msg"] == "OTP Send Successfully !")
            {
                loadingCircle.SetActive(false);
                errorMessage.text = resjson[0]["msg"];
                Debug.Log("test : OTP Send Successfully");

                yield return new WaitForSeconds(2);
                this.GetComponent<LoginSignUpController>().GoToUpdatePassword();
            }
            else
            {
                loadingCircle.SetActive(false);
                errorMessage.text = msg;
            }
        }
    }

    public void OnSendOTPSelected()
    {
        if (validateEmail(email.text))
        {
            loadingCircle.SetActive(true);
            StartCoroutine(callsendotp(email.text));
        }
        else
        {
            errorMessage.text = "Enter a valid Email Id";
        }
    }

    // Resend OTP Function Call
    IEnumerator callresendotp(string email)
    {
        var otpmail = new SendOTPData();
        otpmail.email = email;

        string json = JsonUtility.ToJson(otpmail);
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);

        UnityWebRequest req = new UnityWebRequest(url, "PATCH");

        req.uploadHandler = new UploadHandlerRaw(jsonToSend);
        req.downloadHandler = new DownloadHandlerBuffer();
        req.SetRequestHeader("Content-Type", "application/json");

        yield return req.SendWebRequest();

        if (req.isNetworkError)
        {
            errorMessageResendOTP.text = "Error While Sending: " + req.error;
            Debug.Log("Error While Sending: " + req.error);
        }
        else
        {
            Debug.Log("Received: " + req.downloadHandler.text);
            JSONNode resjson = JSON.Parse(req.downloadHandler.text);

            string msg = resjson[0][0]["msg"];
            Debug.Log(msg);

            if (resjson[0][0]["msg"] == "OTP Send Unsuccessfully")
            {
                errorMessageResendOTP.text = resjson[0][0]["msg"];
                Debug.Log("test : OTP Send Unsuccessfully");
            }
            else if (resjson[0]["msg"] == "OTP Send Successfully !")
            {
                errorMessageResendOTP.text = resjson[0]["msg"];
                Debug.Log("test : OTP Send Successfully");
            }
            else
            {
                errorMessageResendOTP.text = msg;
            }
        }
    }

    public void OnResendOTPSelected()
    {
        StartCoroutine(callresendotp(email.text));
    }
}
