using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeShader : MonoBehaviour
{
    public Material Material1;
    public Material Material2;

    void Start()
    {
        //Material = this.gameObject.GetComponent<Renderer>().materials;
        MakeNormal();
       
    }

    public void HighLight()
    {
        this.gameObject.GetComponent<SkinnedMeshRenderer>().material = Material2;
    }

    public void MakeNormal()
    {
        this.gameObject.GetComponent<SkinnedMeshRenderer>().material = Material1;
    }


    public void delay()
    {
        StartCoroutine(delayShader());
    }

    public IEnumerator delayShader()
    {
        yield return new WaitForSeconds(20f);
        this.gameObject.GetComponent<SkinnedMeshRenderer>().material = Material2;
    }
}
