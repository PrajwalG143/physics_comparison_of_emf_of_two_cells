using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDrag : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if(Input.GetMouseButton(0))
    //    {
    //        var pos = Input.mousePosition;
    //        pos.z = -Camera.main.transform.position.z;
    //        pos = Camera.main.ScreenToWorldPoint(pos);
    //        var dir = pos - transform.position;
    //        this.gameObject.GetComponent<Rigidbody>().AddForce(dir * 10);
    //    }
    //}

    Vector3 mouse_movement;
    public float MouseSensitivity;
    private void Update()
    {
        mouse_movement = GetMouseMovement();
    }

    private void FixedUpdate()
    {
        Vector3 new_position = transform.position -  mouse_movement;
        this.gameObject.GetComponent<Rigidbody>().MovePosition(new_position);
    }

    public Vector3 GetMouseMovement()
    {
        Vector3 mouse_movement = new Vector3(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        return mouse_movement * MouseSensitivity;
    }
}
