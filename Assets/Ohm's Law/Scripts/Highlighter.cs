using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlighter : MonoBehaviour
{
    public GameObject objectToBeHighlighted;
    Material defaultMaterial;

    // Start is called before the first frame update
    void Start()
    {
        defaultMaterial = objectToBeHighlighted.GetComponent<MeshRenderer>().material;
    }


    public void changematerial(Material highlight)
    {
        objectToBeHighlighted.GetComponent<MeshRenderer>().material = highlight;
    }


    public void revertMaterial()
    {
        objectToBeHighlighted.GetComponent<MeshRenderer>().material = defaultMaterial;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
