using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragRheostat : MonoBehaviour
{
    private Vector3 screenPoint;
    private Vector3 offset;
    private Vector3 initialPos;

    private Vector3 maxResistancePos;
    private Vector3 minResistancePos;

    public float currentResiatanceInOhm;
    public float maxRheostatResistance;

    public float initialRotationAngleZ;
    public float initialRotatioVoltageZ;
    
    //public GameObject ikObject;
    public bool shallIUpdatePos = false;
    public bool isDragNow = false;
    public float clampValue = 0.2f;

    public GameObject ammeterNeedle;
    public GameObject voltmeterNeedle;

    public float currentI, voltage;
    public float resistance ;

    
    private void Start()
    {
        resistance = Random.Range(1f, 3f);
        resistance = Mathf.Round(resistance * 10.0f) * 0.1f;

        this.gameObject.GetComponent<Rigidbody>().freezeRotation = true;
        initialPos = this.transform.position;
        maxResistancePos = this.transform.position;
        minResistancePos = new Vector3(maxResistancePos.x - clampValue, maxResistancePos.y, maxResistancePos.z);
        this.transform.position = maxResistancePos;
        currentResiatanceInOhm = (this.transform.position.x - minResistancePos.x) * maxRheostatResistance / clampValue;

        initialRotationAngleZ = ammeterNeedle.transform.rotation.eulerAngles.z;
        initialRotatioVoltageZ = voltmeterNeedle.transform.rotation.eulerAngles.z;



    }

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        isDragNow = true;
        shallIUpdatePos = true;
        
    }

    void OnMouseDrag()
    {
        if (isDragNow && shallIUpdatePos)
        {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            float xpos = Mathf.Clamp(curPosition.x, initialPos.x - clampValue, initialPos.x);
            transform.position = new Vector3(xpos, transform.position.y, transform.position.z);

            currentResiatanceInOhm = (this.transform.position.x - minResistancePos.x) * maxRheostatResistance / clampValue;

            currentI = (100 - currentResiatanceInOhm)/100 * 5 ; 
            currentI = Mathf.Round(currentI * 10.0f) * 0.1f;
            voltage = currentI * resistance ;
            voltage = Mathf.Round(voltage * 10.0f) * 0.1f;



            {
               
                ammeterNeedle.transform.rotation = Quaternion.Euler(ammeterNeedle.transform.rotation.eulerAngles.x,
                                                                ammeterNeedle.transform.rotation.eulerAngles.y,
                                                                initialRotationAngleZ + currentI * 78f/5);
            }


            {
                voltmeterNeedle.transform.rotation = Quaternion.Euler(voltmeterNeedle.transform.rotation.eulerAngles.x,
                                                                voltmeterNeedle.transform.rotation.eulerAngles.y,
                                                                initialRotatioVoltageZ + voltage * 80f/15);
            }
            
        }
    }

    private void Update()
    {
        if (isDragNow && !shallIUpdatePos)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.name == this.name)
                {
                    shallIUpdatePos = true;
                }
            }
        }

        //initialRotationAngleZ = ammeterNeedle.transform.rotation.eulerAngles.z;
    }

    private void OnMouseUp()
    {
        isDragNow = false;
        shallIUpdatePos = false;
        //this.gameObject.GetComponent<Rigidbody>().useGravity = true;
        //ikObject.GetComponent<HoldCube>().isObjectLeft = true;
        // ikObject.GetComponent<HoldCube>().objectHolding = null;
        //ikObject.GetComponent<HoldCube>().startUpdatingPos = false;
    }
    void SetVoltmeterReading(float i, float r){

    }
}
