﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TakeReading : MonoBehaviour
{
    public static TakeReading instance;
    public DragRheostat dragRheostat;
    public TMP_Text[] iText;
    public TMP_Text[] vText;
    public float[] iReading, vReading;
    public int currentReading =0;


    // Start is called before the first frame update
    void Start()
    {
        iReading = new float[5];
        vReading = new float[5];
    }
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void NoteReading()
    {
        if(currentReading<iText.Length)
        {
            print(iReading.Length);
            iText[currentReading].text = dragRheostat.currentI.ToString();
            vText[currentReading].text = dragRheostat.voltage.ToString();
            iReading[currentReading] = dragRheostat.currentI;
            vReading[currentReading] = dragRheostat.voltage;
            
            currentReading++;
        }
    }
    
    public void ResetReading()
    {
        Destroy(gameObject);
    }
}
