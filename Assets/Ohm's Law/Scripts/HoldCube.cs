using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class HoldCube : MonoBehaviour
{
    public FullBodyBipedIK refIK;

    public Transform leftHandTarget;
    public Transform rightHandTarget;

    public Transform leftHandInit;
    public Transform rightHandInit;

    public bool holdSelectedObject = false;

    public bool isObjectLeft = false;

    public float delayTime = 15f;

    public bool startUpdatingPos = false;

    public GameObject objectHolding;

    private void LateUpdate()
    {
        if (holdSelectedObject)
        {
            StartCoroutine(SetHandPosAndRot(delayTime));
            holdSelectedObject = false;
        }

        if (isObjectLeft)
        {
            startUpdatingPos = false;
            StopCoroutine("SetHandPosAndRot");
            StartCoroutine(ResetHandPosAndRot(delayTime));
            isObjectLeft = false;
        }
    
        if (startUpdatingPos)
        {
            //if(objectHolding != null && objectHolding.GetComponent<DragObject>().shallIUpdatePos && objectHolding.GetComponent<DragObject>().isDragNow)
            //{
                refIK.solver.leftHandEffector.position = leftHandTarget.position;
                refIK.solver.rightHandEffector.position = rightHandTarget.position;

                refIK.solver.leftHandEffector.rotation = leftHandTarget.rotation;
                refIK.solver.rightHandEffector.rotation = rightHandTarget.rotation;
            //}
            //else
            //{
            //    startUpdatingPos = false;
            //}
        }
    }

    IEnumerator ResetHandPosAndRot(float duration)
    {
        refIK.solver.leftHandEffector.positionWeight = 1f;
        refIK.solver.leftHandEffector.rotationWeight = 1f;

        refIK.solver.rightHandEffector.positionWeight = 1f;
        refIK.solver.rightHandEffector.rotationWeight = 1f;

        float time = 0;

        Vector3 startPositionLeftHand = refIK.solver.leftHandEffector.position;
        Vector3 startPositionRightHand = refIK.solver.rightHandEffector.position;

        Quaternion startingRotationOfLeftHand = refIK.solver.leftHandEffector.rotation;
        Quaternion startingRotationOfRightHand = refIK.solver.rightHandEffector.rotation;

        while (time < duration)
        {
            refIK.solver.leftHandEffector.position = Vector3.Lerp(startPositionLeftHand, leftHandInit.position, time / duration);
            refIK.solver.rightHandEffector.position = Vector3.Lerp(startPositionRightHand, rightHandInit.position, time / duration);

            refIK.solver.leftHandEffector.rotation = Quaternion.Lerp(startingRotationOfLeftHand, leftHandInit.rotation, time / duration);
            refIK.solver.rightHandEffector.rotation = Quaternion.Lerp(startingRotationOfRightHand, rightHandInit.rotation, time / duration);

            time += Time.deltaTime;

            if (time > duration)
            {
                //refIK.solver.leftHandEffector.positionWeight = 0f;
                //refIK.solver.leftHandEffector.rotationWeight = 0f;

                //refIK.solver.rightHandEffector.positionWeight = 0f;
                //refIK.solver.rightHandEffector.rotationWeight = 0f;

                refIK.solver.leftHandEffector.position = leftHandInit.position;
                refIK.solver.rightHandEffector.position = rightHandInit.position;

                refIK.solver.leftHandEffector.rotation = leftHandInit.rotation;
                refIK.solver.rightHandEffector.rotation = rightHandInit.rotation;
                StopCoroutine("ResetHandPosAndRot");
            }
            yield return null;
        }

    }

    IEnumerator SetHandPosAndRot(float duration)
    {
        refIK.solver.leftHandEffector.positionWeight = 1f;
        refIK.solver.leftHandEffector.rotationWeight = 1f;

        refIK.solver.rightHandEffector.positionWeight = 1f;
        refIK.solver.rightHandEffector.rotationWeight = 1f;

        float time = 0;

        Vector3 startPositionLeftHand = refIK.solver.leftHandEffector.position;
        Vector3 startPositionRightHand = refIK.solver.rightHandEffector.position;

        Quaternion startingRotationOfLeftHand = refIK.solver.leftHandEffector.rotation;
        Quaternion startingRotationOfRightHand = refIK.solver.rightHandEffector.rotation;

        if (!Input.GetMouseButton(0))
        {
            startUpdatingPos = false;
            if (objectHolding != null)
            {
                objectHolding.GetComponent<DragObject>().isDragNow = false;
            }
        }

        while (time < duration)
        {
            refIK.solver.leftHandEffector.position = Vector3.Lerp(startPositionLeftHand, leftHandTarget.position, time / duration);
            refIK.solver.rightHandEffector.position = Vector3.Lerp(startPositionRightHand, rightHandTarget.position, time / duration);

            refIK.solver.leftHandEffector.rotation = Quaternion.Lerp(startingRotationOfLeftHand, leftHandTarget.rotation, time / duration);
            refIK.solver.rightHandEffector.rotation = Quaternion.Lerp(startingRotationOfRightHand, rightHandTarget.rotation, time / duration);

            time += Time.deltaTime;

            if (time > duration)
            {
                if(Input.GetMouseButton(0))
                {
                    startUpdatingPos = true;
                    if (objectHolding != null)
                    {
                        objectHolding.GetComponent<DragObject>().isDragNow = true;
                    }
                    
                }
                refIK.solver.leftHandEffector.position = leftHandTarget.position;
                refIK.solver.rightHandEffector.position = rightHandTarget.position;

                refIK.solver.leftHandEffector.rotation = leftHandTarget.rotation;
                refIK.solver.rightHandEffector.rotation = rightHandTarget.rotation;
                StopCoroutine("SetHandPosAndRot");
            }
            yield return null;
        }


    }
}




