﻿using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;
using System.Collections.Generic;

public class SQLCrud : MonoBehaviour
{
    [SerializeField]
    public List<QAClass> QuestionAnswers;

    public GameObject MCQHandler;
    string connection;
    // Start is called before the first frame update
    void Start()
    {
        QuestionAnswers = new List<QAClass>();

        /*
        connection = Application.persistentDataPath + "/MCQDB.db";
        if (!File.Exists(connection))
        {
            Debug.Log("DB file does not exist");
            //Open Streaming assets and load the db
            WWW loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/MCQDB.db");
            while (!loadDb.isDone) { }
            File.WriteAllBytes(connection, loadDb.bytes);
        }
        else
        {
            Debug.Log("File exists");
        }
        //Once file is loaded, use the appropiate filepath to access db
        connection = "URI=file:"+connection;
        */

        connection = "URI=file:" + Application.streamingAssetsPath + "/MCQDB.db ";

        IDbConnection dbcon = new SqliteConnection(connection);
        dbcon.Open();

        IDbCommand dbcmd;
        IDataReader reader;

        //dbcmd = dbcon.CreateCommand();
        //string q_createTable =
         // "CREATE TABLE IF NOT EXISTS my_table (id INTEGER PRIMARY KEY, val INTEGER )";

        //dbcmd.CommandText = q_createTable;
        //reader = dbcmd.ExecuteReader();

        //IDbCommand cmnd = dbcon.CreateCommand();
        //cmnd.CommandText = "INSERT INTO my_table (id, val) VALUES (0, 5)";
        //cmnd.ExecuteNonQuery();

        IDbCommand cmnd_read = dbcon.CreateCommand();
       
        string query = "SELECT * FROM QuestionAnswers";
        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();
        while (reader.Read())
        {
            QAClass qa = new QAClass();
            qa.QNo = int.Parse(reader[0].ToString());
            qa.Question = reader[1].ToString();
            qa.OptionA = reader[2].ToString();
            qa.OptionB = reader[3].ToString();
            qa.OptionC = reader[4].ToString();
            qa.OptionD = reader[5].ToString();
            qa.AnsKey = reader[6].ToString();
            QuestionAnswers.Add(qa);
            Debug.Log(qa.ToString());
            
        }
        if(QuestionAnswers!=null && QuestionAnswers.Count>0)
        {
            MCQHandler.SendMessage("SetUpQAList", QuestionAnswers, SendMessageOptions.DontRequireReceiver);
          
        }
        dbcon.Close();


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*
    void Start()
    {
        QuestionAnswers = new List<QAClass>();

        connection = "URI=file:" + Application.streamingAssetsPath + "/MCQDB.db";
        
        IDbConnection dbcon = new SqliteConnection(connection);
        dbcon.Open();

        IDbCommand dbcmd;
        IDataReader reader;

        //dbcmd = dbcon.CreateCommand();
        //string q_createTable =
         // "CREATE TABLE IF NOT EXISTS my_table (id INTEGER PRIMARY KEY, val INTEGER )";

        //dbcmd.CommandText = q_createTable;
        //reader = dbcmd.ExecuteReader();

        //IDbCommand cmnd = dbcon.CreateCommand();
        //cmnd.CommandText = "INSERT INTO my_table (id, val) VALUES (0, 5)";
        //cmnd.ExecuteNonQuery();

        IDbCommand cmnd_read = dbcon.CreateCommand();
       
        string query = "SELECT * FROM QuestionAnswers";
        cmnd_read.CommandText = query;
        reader = cmnd_read.ExecuteReader();
        while (reader.Read())
        {
            QAClass qa = new QAClass();
            qa.QNo = int.Parse(reader[0].ToString());
            qa.Question = reader[1].ToString();
            qa.OptionA = reader[2].ToString();
            qa.OptionB = reader[3].ToString();
            qa.OptionC = reader[4].ToString();
            qa.OptionD = reader[5].ToString();
            qa.AnsKey = reader[6].ToString();
            QuestionAnswers.Add(qa);
            Debug.Log(qa.ToString());
            
        }
        if(QuestionAnswers!=null && QuestionAnswers.Count>0)
        {
            MCQHandler.SendMessage("SetUpQAList", QuestionAnswers, SendMessageOptions.DontRequireReceiver);
          
        }
        dbcon.Close();


    }
    */
}
