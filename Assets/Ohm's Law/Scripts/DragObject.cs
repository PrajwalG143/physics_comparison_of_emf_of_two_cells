using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    public GameObject ikObject;
    private Vector3 screenPoint;
    private Vector3 offset;

    public bool shallIUpdatePos = false;
    
    public bool isDragNow = false;

    public GameObject currentHitObject;

    public float hitDistance;

    private void Start()
    {
        this.gameObject.GetComponent<Rigidbody>().freezeRotation = true;
    }

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

        this.gameObject.GetComponent<Rigidbody>().useGravity = false;

        ikObject.GetComponent<HoldCube>().holdSelectedObject = true;

        ikObject.GetComponent<HoldCube>().isObjectLeft = false;
        
        ikObject.GetComponent<HoldCube>().objectHolding = this.gameObject;

        isDragNow = false;

        shallIUpdatePos = false;
    }

    void OnMouseDrag()
    {
        if (isDragNow && shallIUpdatePos)
        {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;

            transform.position = Vector3.Lerp(transform.position, curPosition, 0.1f);//curPosition;
        }
    }

    private void Update()
    {
        if (isDragNow && !shallIUpdatePos)
        {
            RaycastHit hit;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.name == this.name)
                {
                    shallIUpdatePos = true;
                }
            }
        }
    }

    private void OnMouseUp()
    {
        this.gameObject.GetComponent<Rigidbody>().useGravity = true;
        
        ikObject.GetComponent<HoldCube>().isObjectLeft = true;

        ikObject.GetComponent<HoldCube>().objectHolding = null;

        ikObject.GetComponent<HoldCube>().startUpdatingPos = false;

        isDragNow = false;

        shallIUpdatePos = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (currentHitObject != null && collision.gameObject.name == currentHitObject.name)
        {
            isDragNow = false;
            Invoke("EnableDrag", 0.1f);
        }
    }

    void EnableDrag()
    {
        isDragNow = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (currentHitObject != null &&  collision.gameObject.name == currentHitObject.name)
        {
            isDragNow = true;
        }
    }
}
