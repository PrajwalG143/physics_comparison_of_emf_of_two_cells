using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class HoldPin : MonoBehaviour
{
    public FullBodyBipedIK refIK;

    public Transform rightHandTarget;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //refIK.solver.leftHandEffector.positionWeight = 1f;
        //refIK.solver.leftHandEffector.rotationWeight = 1f;

        refIK.solver.rightHandEffector.positionWeight = 1f;
        refIK.solver.rightHandEffector.rotationWeight = 1f;

        //refIK.solver.leftHandEffector.position = leftHandTarget.position;
        refIK.solver.rightHandEffector.position = rightHandTarget.position;

        //  refIK.solver.leftHandEffector.rotation = leftHandTarget.rotation;
        refIK.solver.rightHandEffector.rotation = rightHandTarget.rotation;
    }
}
