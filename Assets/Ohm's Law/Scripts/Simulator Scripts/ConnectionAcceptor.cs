using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Lean;
using Lean.Touch;

public class ConnectionAcceptor : MonoBehaviour
{
    #region public non serialized variables

    //[NonSerialized]
    public bool isThisConnectorConnected;
    //[NonSerialized]
    public bool isSecondConnDone;

    //[NonSerialized]
    public GameObject thisConnConnectedPin;

    #endregion

    #region public variables   

    public Vector3 posOffsetValues;

    public float yOffsetForPin;

    public int maxNoOfConnections;

    public float yOffsetForPinRemoval;

    public Vector3 rotOffsetAngles;

    #endregion

    #region private variables

    private Vector3 newPos;
    private PinConnectionDetails pinConnDetails;
    private Rigidbody pinRigidBody;
    private DragPin pinDragPin;
    private Collider pinCollider;

    private bool collision_entered = false;
    #endregion

    #region Follow Child Part
    Transform follow_conn;
    LeanDragTranslateAlong leanDragTranslateAlong_script;
    #endregion
    void Start()
    {
        isThisConnectorConnected = false;
        //newPos = new Vector3(posOffsetValues.x, posOffsetValues.y, posOffsetValues.z);
        follow_conn = transform.Find("follow_conn");
    }

    private void Awake()
    {
        temp = true;
    }

    #region connection checking and status updating functions

    private void OnCollisionEnter(Collision collision)
    {
        collision_entered = true;
        ConnectionManager.MyInstance.UpdateUIText("Pin Entered Connector collider isThisConnectorConnected");
        if (!isThisConnectorConnected && collision.gameObject.tag.Contains("Pin"))
        {
            temp = false;
            isThisConnectorConnected = true;
            thisConnConnectedPin = collision.gameObject;

            pinDragPin = collision.gameObject.GetComponent<DragPin>();
            pinDragPin.shallIUpdatePos = false;

            newPos = new Vector3(this.gameObject.transform.position.x,
                                 this.gameObject.transform.position.y + yOffsetForPin,
                                 this.gameObject.transform.position.z);

            pinCollider = collision.gameObject.GetComponent<Collider>();
            Debug.Log("Disabling Collider");
            pinCollider.enabled = false;

            pinRigidBody = collision.gameObject.GetComponent<Rigidbody>();
            pinRigidBody.useGravity = false;
            pinRigidBody.isKinematic = true;


            ConnectionManager.MyInstance.UpdateUIText("Pin Entered Connector collider !isThisConnectorConnected");
            if (thisConnConnectedPin.transform.Find("follow") != null)
            {
                make_pin_orientation();

            }
            else
            {
                thisConnConnectedPin.transform.rotation = Quaternion.Euler(0, 0, 0);
                thisConnConnectedPin.transform.Rotate(rotOffsetAngles.x, rotOffsetAngles.y, rotOffsetAngles.z);
                thisConnConnectedPin.transform.position = newPos;
                thisConnConnectedPin.transform.Translate(posOffsetValues, Space.Self);

            }
            #region Added Part
             leanDragTranslateAlong_script = thisConnConnectedPin.GetComponent<LeanDragTranslateAlong>();

            leanDragTranslateAlong_script.enabled = false;

            if(thisConnConnectedPin.name== "Bannana_pin_jockey" && this.name== "Gal_Neg_Terminal")
            {
                //galv_jockey_connection_pins galv_Jockey_Connection_Pins_script = this.GetComponent<galv_jockey_connection_pins>();
                //if (galv_Jockey_Connection_Pins_script)
                //    galv_jockey_connection_pins.complete_connection_with_jockey = true;
                ConnectionManager.MyInstance.complete_connection_with_jockey = true;
                thisConnConnectedPin.GetComponent<DragPin>().isPair_connected = true;

            }
            #endregion


            pinConnDetails = collision.gameObject.GetComponent<PinConnectionDetails>();
            pinConnDetails.UpdatePinConnDetails(true, this.gameObject);
            pinConnDetails.ApplyFixingConstraints();

            if (pinConnDetails.secondPin != null)
            {
                pinConnDetails.SecondPinStateSetter();
            }
            Invoke("UpdatePosAgain", 0.1f);
            Invoke("EnableCollider", 0.1f);
        }
    }

    #region on stay collision
    private void OnCollisionStay(Collision collision)
    {
        //if (thisConnConnectedPin != null)
        //    return;

        ////Added Line to Ensure the connection
        //thisConnConnectedPin = collision.gameObject;
    }
    #endregion

    //New Function Added to make the orientation of the Bannana pins 
    void make_pin_orientation()
    {
        Transform follow = thisConnConnectedPin.transform.Find("follow");

        Follow follow_script = thisConnConnectedPin.GetComponent<Follow>();

        //follow.position = new Vector3(this.gameObject.transform.position.x,
        //                         this.gameObject.transform.position.y + yOffsetForPin,
        //                         this.gameObject.transform.position.z);
        //follow.transform.right = -transform.forward;
        //follow_script.move_parent_relative_toChild();
        //follow_script.update_location_and_rotation();
        if (follow_conn != null)
        {
            follow.transform.right = -follow_conn.transform.up;
            follow.position = new Vector3(follow_conn.transform.position.x,
                                 follow_conn.transform.position.y + yOffsetForPin,
                                 follow_conn.transform.position.z);
        }
        else
        {
            follow.transform.right = transform.up;
            follow.position = new Vector3(this.gameObject.transform.position.x,
                                 this.gameObject.transform.position.y + yOffsetForPin,
                                 this.gameObject.transform.position.z);

        }

        follow_script.follow_child_func();

        //follow_script.move_parent_relative_toChild();
        //follow_script.update_location_and_rotation();


    }
    void UpdatePosAgain()
    {
        if (isThisConnectorConnected && thisConnConnectedPin != null)
        {
            thisConnConnectedPin.transform.position = thisConnConnectedPin.transform.position;
            // thisConnConnectedPin.transform.position = thisConnConnectedPin.transform.position;
            thisConnConnectedPin.transform.rotation = thisConnConnectedPin.transform.rotation;
            //Debug.Log("Disabling Collider");
            pinCollider.enabled = true;
            pinRigidBody.isKinematic = false;
            Invoke("EnableCollider", 0.1f);
        }
    }

    void EnableCollider()
    {
        //Debug.Log("Enabling Collider again");
        pinCollider.enabled = true;
        pinRigidBody.isKinematic = false;
    }

    public void ResetDetails()
    {
        //Debug.Log(this.name + " resetting");
        if(thisConnConnectedPin!=null)
            if (thisConnConnectedPin.name == "Bannana_pin_jockey" && this.name == "Gal_Neg_Terminal")
            {
         

                ConnectionManager.MyInstance.complete_connection_with_jockey = false;
                //thisConnConnectedPin.GetComponent<DragPin>().isPair_connected = false;
            }
        isThisConnectorConnected = false;
        thisConnConnectedPin = null;
    }

    public bool temp;

    /*
    private void Update()
    {
        if (temp)
        {
            //Debug.Log("Laka Laka Laka Laka");
            ResetDetails();
        }
    }
    */

    private void OnCollisionExit(Collision collision)
    {
        temp = true;
        ResetDetails();
    }

    #endregion
}