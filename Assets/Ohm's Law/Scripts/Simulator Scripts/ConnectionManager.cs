using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.SceneManagement;
using Lean.Touch;

public class ConnectionManager : MonoBehaviour
{
    private static ConnectionManager _myInstance;

    public static ConnectionManager MyInstance
    {
        get => _myInstance;
    }


    [Serializable]
    public struct ValidConnectionPair
    {
        public GameObject pointOne;
        public GameObject pointTwo;
    }

    #region Instruction panel
    int inst_1_count = 0,inst_6_count=0;
    #endregion
    private void Awake()
    {
        if (_myInstance == null)
        {
            _myInstance = this;
        }
        else
        {
            Debug.Log("Myinstance is already existing");
        }
    }

    /// <summary>
    /// /////////////////////////////////////////////////////////////////////////////////////////////Variables and Lists ( public )
    /// </summary>

    private bool isCorrectConnIdentified = false;
    //[SerializeField]
    private Material highlight;
    public TextMeshPro UI;
    public TextMeshPro count;

    public ValidConnectionPair[] properConnectionPairs;


    public List<ValidConnectionPair> already_connected_pairs;
    public int totalProperConnectionsDone;

    [SerializeField]
    private Transform Instruction_ScreenSpace;
    Dictionary<int,bool> val_pair;
    #region Connection added
    public bool allConnected;
    //to check if single terminal of jockey wire is connected
    public bool complete_connection_with_jockey = false;
    public Dictionary<GameObject, List<GameObject>> alternateConnections;
    #endregion
    void Start()
    {
        if(UI!=null)
        UI.text = "";
        if(count!=null)
        count.text = "000";

        #region added
        allConnected = false;
        complete_connection_with_jockey = false;
        val_pair = new Dictionary<int, bool>();

        alternateConnections = new Dictionary<GameObject, List<GameObject>>();

        already_connected_pairs = new List<ValidConnectionPair>();
        #endregion
        if (properConnectionPairs.Length > 0&& highlight!=null)
        {
            //properConnectionPairs[totalProperConnectionsDone].pointOne.GetComponent<Highlighter>().changematerial(highlight);
            //properConnectionPairs[totalProperConnectionsDone].pointTwo.GetComponent<Highlighter>().changematerial(highlight);
        }
        #region instruction
        ////ENABLE 1ST INSTRUCTION
        //enable_instruction(0);
        ////AFTER SOME TIME ENABLE 2ND (CONNECTION INSTRUCTION)
        //StartCoroutine(  display_text_for_few_secs(0, 5));
        #endregion
    }


    #region connection status checking functions

    public bool CheckConnection(GameObject object1, GameObject object2)
    {
        List<GameObject> pins;


        UpdateUIText("entered Check Connection");
        isCorrectConnIdentified = false;
        foreach (ValidConnectionPair obj in properConnectionPairs)
        {
            GameObject obj_pointOne=null, obj_pointTwo=null;
            if (alternateConnections != null)
            {
                if (alternateConnections.Count > 0)
                {
                    //if (alternateConnections.ContainsKey(object1))
                    if (alternateConnections.ContainsKey(object1))
                    {



                       if( alternateConnections[object1].Contains(obj.pointOne))
                        {
                            obj_pointOne =object1 ;// obj.pointOne;
                        }
                        else if (alternateConnections[object1].Contains(obj.pointTwo))
                        {
                            obj_pointOne = object1;// obj.pointOne;
                        }
                    }
                    //else if (alternateConnections.ContainsKey(object2))
                     if (alternateConnections.ContainsKey(object2))
                    {
                        
                        if (alternateConnections[object2].Contains(obj.pointTwo))
                        {
                            obj_pointTwo = object2;// obj.pointTwo;
                        }
                        else if (alternateConnections[object2].Contains(obj.pointOne))
                        {
                            obj_pointOne = object2;// obj.pointOne;
                        }

                    }
                }
            }
            if(obj_pointOne==null)
            {
                obj_pointOne = obj.pointOne;
            }
            if(obj_pointTwo==null)
            {
                obj_pointTwo = obj.pointTwo;
            }
            //if ((obj.pointOne == object1 && obj.pointTwo == object2) || (obj.pointOne == object2 && obj.pointTwo == object1))
            if ((obj_pointOne == object1 && obj_pointTwo == object2) || (obj_pointOne == object2 &&obj_pointTwo == object1))
            {
                if(!already_connected_pairs.Contains(obj))
                {
                    already_connected_pairs.Add(obj);
                isCorrectConnIdentified = true;
                }

                //if(true)
                //{
                //    //TO SHOW STEPS
                //    //proper_connection_done_steps(obj);
                //}
                if (!alternateConnections.ContainsKey(object1))
                {

                    pins = new List<GameObject>();
                    pins.Add(object2);
                    pins.Add(object1);
                    if(!pins.Contains(obj.pointOne))
                    {
                        pins.Add(obj.pointOne);
                    }
                    if(!pins.Contains(obj.pointTwo))
                    {
                        pins.Add(obj.pointTwo);
                    }    
                    if (!alternateConnections.ContainsKey(object1))
                    {
                        alternateConnections.Add(object1, pins);
                    }
                    else
                    {
                        alternateConnections[object1] = pins;
                    }
                }
                else
                {
                    pins = alternateConnections[object1];

                    if(!pins.Contains(object1))
                    pins.Add(object1);


                    if (!pins.Contains(obj.pointOne))
                    {
                        pins.Add(obj.pointOne);
                    }
                    if (!pins.Contains(obj.pointTwo))
                    {
                        pins.Add(obj.pointTwo);
                    }
                    if (!alternateConnections.ContainsKey(object1))
                    {
                        alternateConnections.Add(object1, pins);
                    }
                    else
                    {
                        alternateConnections[object1] = pins;
                    }
                }

                if (!alternateConnections.ContainsKey(object2))
                {

                     pins = new List<GameObject>();

                    pins.Add(object2);
                    pins.Add(object1);

                    if (!pins.Contains(obj.pointOne))
                    {
                        pins.Add(obj.pointOne);
                    }
                    if (!pins.Contains(obj.pointTwo))
                    {
                        pins.Add(obj.pointTwo);
                    }
                    //alternateConnections.Add(object2, pins);
                    if (!alternateConnections.ContainsKey(object2))
                    {
                        alternateConnections.Add(object2, pins);
                    }
                    else
                    {
                        alternateConnections[object2] = pins;
                    }
                }
                else
                {
                    pins = alternateConnections[object2];

                    if (!pins.Contains(object2))
                        pins.Add(object2);


                    if (!pins.Contains(obj.pointOne))
                    {
                        pins.Add(obj.pointOne);
                    }
                    if (!pins.Contains(obj.pointTwo))
                    {
                        pins.Add(obj.pointTwo);
                    }
                    if (!alternateConnections.ContainsKey(object2))
                    {
                        alternateConnections.Add(object2, pins);
                    }
                    else
                    {
                        alternateConnections[object2] = pins;
                    }
                }
                try
                {
                    if (object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin != null)
                    {
                        DragPin dragPin_script_object1 = object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<DragPin>();
                        if (dragPin_script_object1!=null)
                        {
                            dragPin_script_object1.isPair_connected = true;

                            //dragPin_script_object1.GetComponent<PinConnectionDetails>().thisPairOtherPin.GetComponent<DragPin>().isPair_connected = true;
                            
                        }
                    }
                    if (object2.GetComponent<ConnectionAcceptor>().thisConnConnectedPin != null)
                    {
                        DragPin dragPin_script_object2 = object2.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<DragPin>();
                        if (dragPin_script_object2!=null)
                        {
                            dragPin_script_object2.isPair_connected = true;

                            //dragPin_script_object2.GetComponent<PinConnectionDetails>().thisPairOtherPin.GetComponent<DragPin>().isPair_connected = true;
                        }
                    }
                }
                catch(Exception ex)
                {
                    Debug.Log("is pair connected :" + ex);
                }
                //if (object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<Collider>())
                //{
                //    object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<Collider>().enabled = false;

                //    //Destroy(object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<Collider>());
                //}
                //if (object2.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<Collider>())
                //{
                //    object2.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<Collider>().enabled = false;
                //}

                //if ( object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<PinConnectionDetails>())
                //{
                //    object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<PinConnectionDetails>().enabled = false;
                //}
                //if (object2.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<PinConnectionDetails>())
                //{
                //    object2.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<PinConnectionDetails>().enabled = false;
                //}


                //if (object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<LeanSelectableByFinger>())
                //{
                //    object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<LeanSelectableByFinger>().enabled = false;
                //}
                //if (object2.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<LeanSelectableByFinger>())
                //{
                //    object2.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<LeanSelectableByFinger>().enabled = false;
                //}

                //if (object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<DragPin>())
                //{
                //    object1.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<DragPin>().enabled = false;
                //}
                //if (object2.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<DragPin>())
                //{
                //    object2.GetComponent<ConnectionAcceptor>().thisConnConnectedPin.GetComponent<DragPin>().enabled = false;
                //}
                break;
            }

            else
            {
                continue;
            }
        }

        if (isCorrectConnIdentified)
        {

            UpdateUIText("Checked cable connection is valid");
            totalProperConnectionsDone++;
            if (totalProperConnectionsDone > 0)
            {
                //properConnectionPairs[totalProperConnectionsDone - 1].pointOne.GetComponent<Highlighter>().revertMaterial();
                //properConnectionPairs[totalProperConnectionsDone - 1].pointTwo.GetComponent<Highlighter>().revertMaterial();
            }
           
            //if(totalProperConnectionsDone <5 && SceneManager.GetActiveScene().name == "TrainingCC")
            //{
               // properConnectionPairs[totalProperConnectionsDone - 1].pointOne.GetComponent<BoxCollider>().gameObject.SetActive(true);
               // properConnectionPairs[totalProperConnectionsDone - 1].pointTwo.GetComponent<BoxCollider>().gameObject.SetActive(true);
           // }

            if (totalProperConnectionsDone < 5)
            {
                //properConnectionPairs[totalProperConnectionsDone].pointOne.GetComponent<Highlighter>().changematerial(highlight);
                //properConnectionPairs[totalProperConnectionsDone].pointTwo.GetComponent<Highlighter>().changematerial(highlight);
            }
            if(count!=null)
            count.text = totalProperConnectionsDone.ToString();
        }
        else
        {
            UpdateUIText("Checked cable connection is invalid");
            if(count!=null)
            count.text = totalProperConnectionsDone.ToString();
        }

        if (totalProperConnectionsDone == properConnectionPairs.Length)
        {

                allConnected = true;
             Jockey_Text_updation.jockey_movt_enable(true);
                //if(UI!=null)
                // UI.text = "Cable connections completed";
            
            if (UI!=null)
            UI.text = "Cable connections completed";
        }
        else
        {
            allConnected = false;
            Jockey_Text_updation.jockey_movt_enable(false);
        }

        return isCorrectConnIdentified;
    }

    #endregion



    #region UI data changing function

    public void UpdateUIText(string inputData)
    {
        if(UI!=null)
        UI.text = inputData;
    }





    #endregion

    
    void proper_connection_done_steps(ValidConnectionPair obj)
    {
      int ind=Array.IndexOf( properConnectionPairs,(obj));


        if(!val_pair.ContainsKey(ind))
        {

        }
        int instruction_no = 0;

         //When 1st 3 pins are connected the 2nd instruction should be displayed
    
        if (ind<=2)
        {

            inst_1_count++;
            if(inst_1_count > 3)
            {
                inst_1_count = 3;
            }                                                                                                                                                                                              
            instruction_no = 1;

        }
       
        else  if(ind==3)
        {
            instruction_no = 4;
        }
        else if(ind==4)
        {
            instruction_no = 5;
        }
        //When 1st 3 pins are connected to T1,T2,T3 6th instruction should be displayed
        else if (ind>=5&& ind <= 7)
        {
            inst_6_count++;
            if(inst_6_count>3)
            {
                inst_6_count = 3;
            }

            //instruction_no = 6;
        }

        //1st instruction
        if (inst_1_count == 3 && ind <= 2)
        {


            instruction_no = 2;
        }

        //6st instruction
        if (inst_6_count == 3 && (ind >= 5 && ind <= 7))
        {


            instruction_no = 6;
        }
        enable_instruction(instruction_no);
        if(instruction_no==3||instruction_no==6)
        {
           StartCoroutine( display_text_for_few_secs(instruction_no,4f));
        }
    }

   public  void enable_instruction(int index_instruction)
    {
       for(int i=0;i<Instruction_ScreenSpace.childCount;i++)
        {
            if(i==index_instruction)
            {
                Instruction_ScreenSpace.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                Instruction_ScreenSpace.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    IEnumerator display_text_for_few_secs(int index_instruction,float secs=1f)
    {

        index_instruction++;
      yield  return new WaitForSeconds(secs);
        enable_instruction(index_instruction);
    }
}
