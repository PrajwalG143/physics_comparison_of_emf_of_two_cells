using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Lean.Touch;

public class PinConnectionDetails : MonoBehaviour
{
    #region public nonserialized variables
   
    [NonSerialized]
    public bool isThisPinConnected;
    [NonSerialized]
    public bool isThisIndirectConnPin;
    [NonSerialized]
    public bool isProperConnCountIncremented;
    [NonSerialized]
    public bool isSecondPinConnected;

    //[NonSerialized]
    public GameObject thisPinConnectedConnector;
    [NonSerialized]
    public GameObject secondPin;

    [NonSerialized]
    public float thresholdYDistance;

    [NonSerialized]
    public Transform mainParent;

    #endregion

    #region private variables

    private Rigidbody thisRigidbody;

    private RigidbodyConstraints originalConstraints;
    private RigidbodyConstraints updatedConstraints;

    private PinConnectionDetails otherPinConnDetails;
    private PinConnectionDetails secondPinConnDetails;

    private Quaternion initialRotation;

    private Vector3 newPos;

    #endregion

    #region public variables

    public float secondPinOffsetMultiplier;

    public GameObject thisPairOtherPin;

    LeanDragTranslateAlong leanDragTranslateAlong_script;
    #endregion

    void Start()
    {
        isThisPinConnected = false;
        isProperConnCountIncremented = false;
        thisRigidbody = this.GetComponent<Rigidbody>();
        thisRigidbody.freezeRotation = true;
        originalConstraints = thisRigidbody.constraints;
        thresholdYDistance = 0;
        
        otherPinConnDetails = thisPairOtherPin.gameObject.GetComponent<PinConnectionDetails>();

        mainParent = this.transform.parent;
        initialRotation = this.transform.rotation;
        leanDragTranslateAlong_script = this.GetComponent<LeanDragTranslateAlong>();
    }

    #region pin data updating funcions

    public void UpdatePinConnDetails(bool _isThisPinConnected, GameObject _thisPinConnectedConnector)
    {
        if(!isThisPinConnected)
        {
            isThisPinConnected = _isThisPinConnected;
            thisPinConnectedConnector = _thisPinConnectedConnector;
            thresholdYDistance = thisPinConnectedConnector.transform.localScale.y;

            if(otherPinConnDetails.isThisPinConnected)
            {
               // ConnectionManager.MyInstance.UpdateUIText("This pin and other pin also connected");
                if(ConnectionManager.MyInstance.CheckConnection(thisPinConnectedConnector, otherPinConnDetails.thisPinConnectedConnector))
                {
                    this.isProperConnCountIncremented = true;
                    otherPinConnDetails.isProperConnCountIncremented = true;
                }
            }
            else
            {
                ConnectionManager.MyInstance.UpdateUIText("This pin is connected but the other pin is not connected");
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        //isThisPinConnected = false;
        //thisPinConnectedConnector = null;

        //ConnectionManager.MyInstance.totalProperConnectionsDone--;
        //if (ConnectionManager.MyInstance.count.text != null)
        //    ConnectionManager.MyInstance.count.text = ConnectionManager.MyInstance.totalProperConnectionsDone.ToString();


    }
    public void ResetPinConnDetails(bool _isThisPinRemoved)
    {
        if (isThisPinConnected)
        {
            isThisPinConnected = _isThisPinRemoved;
            thisPinConnectedConnector = null;
            if(!isThisIndirectConnPin)
            {
                thresholdYDistance = 0;
            }
            if(otherPinConnDetails.isThisPinConnected)
            {
                ConnectionManager.MyInstance.UpdateUIText("This pin is disconnected, but the other pin is connected");
                if(isProperConnCountIncremented && otherPinConnDetails.isProperConnCountIncremented)
                {
                    isProperConnCountIncremented = false;
                    otherPinConnDetails.isProperConnCountIncremented = false;
                    ConnectionManager.MyInstance.totalProperConnectionsDone--;
                    if(ConnectionManager.MyInstance.count.text!=null)
                    ConnectionManager.MyInstance.count.text = ConnectionManager.MyInstance.totalProperConnectionsDone.ToString();
                }
            }
            else
            {
                //ConnectionManager.MyInstance.UpdateUIText("This pin disconnected, other pin also disconnected... Entire wire is disassembled now");
            }
        }
    }

    #endregion

    #region pin motion controlling functions

    public void ApplyFixingConstraints()
    {
        thisRigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
        thisRigidbody.freezeRotation = true;
        updatedConstraints = thisRigidbody.constraints;
    }

    public void ApplyResettingConstraints()
    {
        thisRigidbody.constraints = originalConstraints;
    }

    public void StartCoroutineMPU(float param)
    {
        StartCoroutine(MovePinUp(param));
    }

    public void StartCoroutineMPR(float param)
    {
        StartCoroutine(MovePinSide(param));
    }

    #endregion

    #region pin moving and state changing coroutines

    public IEnumerator MovePinUp(float delayTime)
    {
        float localCount = thresholdYDistance / 10;
        float offset = thresholdYDistance / 10;

        #region new Lines for offset 

        //MeshFilter mf = thisPinConnectedConnector.GetComponentInChildren<MeshFilter>();//.mesh.bounds.size

        MeshFilter mf = GetComponentInChildren<MeshFilter>();//.mesh.bounds.size
        Vector3 size = mf.mesh.bounds.size;
        float max = Mathf.Max(size.x, size.y, size.z);

        offset = max * 4f;

        //if(offset< 0.0159363/2)
        //{
        //    offset = 0.0159363f/2;
        //}
        #endregion

        //Added Line
        if (leanDragTranslateAlong_script != null)
            leanDragTranslateAlong_script.enabled = true;

        thisRigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        thisRigidbody.freezeRotation = true;
        if (isThisPinConnected)
        {
         

            this.gameObject.GetComponent<Collider>().enabled = false;
            this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            while (localCount <= thresholdYDistance)
            {
                localCount += (thresholdYDistance / 10);
                //this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 2.2f*0.01f * offset, this.transform.position.z);
                this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + offset, this.transform.position.z);
                if (localCount > thresholdYDistance)
                {
                    thisPinConnectedConnector.GetComponent<ConnectionAcceptor>().isThisConnectorConnected = false;
                    thisPinConnectedConnector.GetComponent<ConnectionAcceptor>().thisConnConnectedPin = null;
                    thisPinConnectedConnector.GetComponent<ConnectionAcceptor>().isSecondConnDone = false;
                    if (secondPin != null)
                    {
                        secondPin.GetComponent<PinConnectionDetails>().ResetPinConnDetails(false);
                        secondPin.GetComponent<PinConnectionDetails>().Invoke("EnableCollider", 0.5f);
                    }
                    ResetPinConnDetails(false);
                    Invoke("EnableCollider", 0.5f);
                    StopCoroutine("MovePinUp");
                }
                yield return new WaitForSeconds(delayTime);
            }
        }
    }

    public IEnumerator MovePinSide(float delayTime)
    {
        float localCount = thresholdYDistance / 10;
        float offset = thresholdYDistance / 10;

        MeshFilter mf = GetComponentInChildren<MeshFilter>();//.mesh.bounds.size
        Vector3 size = mf.mesh.bounds.size;
        float max = Mathf.Max(size.x, size.y, size.z);

        offset = max * 4f;

        //Added Line
        if (leanDragTranslateAlong_script != null)
            leanDragTranslateAlong_script.enabled = true;

        thisRigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
        thisRigidbody.freezeRotation = true;
        if (isThisIndirectConnPin)
        {
         

            this.gameObject.GetComponent<Collider>().enabled = false;
            this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            while (localCount <= thresholdYDistance)
            {
                localCount += (thresholdYDistance / 10);
                //this.transform.position = new Vector3(this.transform.position.x - 2 * offset, this.transform.position.y, this.transform.position.z);
                this.transform.position = new Vector3(this.transform.position.x -  offset, this.transform.position.y, this.transform.position.z);
                if (localCount > thresholdYDistance)
                {
                    isThisIndirectConnPin = false;
                    if(thisPinConnectedConnector != null)
                    {
                        thisPinConnectedConnector.GetComponent<ConnectionAcceptor>().isSecondConnDone = false;
                    }
                    this.transform.parent.GetComponent<PinConnectionDetails>().isSecondPinConnected = false;
                    ResetPinConnDetails(false);
                    Invoke("EnableCollider", 0.5f);
                    StopCoroutine("MovePinSide");
                    this.transform.parent.GetComponent<PinConnectionDetails>().secondPin = null;
                    this.transform.parent = mainParent;
                    this.transform.rotation = initialRotation;
                    isThisIndirectConnPin = false;
                }
                yield return new WaitForSeconds(delayTime);
            }
        }
    }

    void EnableCollider()
    {
        ApplyResettingConstraints();
        thisRigidbody.useGravity = false;
        this.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        this.gameObject.GetComponent<Collider>().enabled = true;
    }

    #endregion

    #region second pin connection controllers

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Contains("Pin") && isThisPinConnected && !collision.gameObject.GetComponent<PinConnectionDetails>().isThisPinConnected
            && thisPinConnectedConnector.GetComponent<ConnectionAcceptor>().maxNoOfConnections > 1 && !isSecondPinConnected && 
            !thisPinConnectedConnector.GetComponent<ConnectionAcceptor>().isSecondConnDone)
        {
            thisPinConnectedConnector.GetComponent<ConnectionAcceptor>().isSecondConnDone = true;
            isSecondPinConnected = true;
            secondPin = collision.gameObject;
            SecondPinStateSetter();
        }
    }

    public  void SecondPinStateSetter()
    {
        secondPin.gameObject.GetComponent<DragPin>().shallIUpdatePos = false;

        if(secondPin.gameObject.GetComponent<Collider>()!=null)
        secondPin.gameObject.GetComponent<Collider>().enabled = false;

        secondPin.gameObject.GetComponent<Rigidbody>().useGravity = false;
        secondPin.gameObject.GetComponent<Rigidbody>().isKinematic = true;

        secondPin.gameObject.transform.position = this.gameObject.transform.position;
        secondPin.gameObject.transform.rotation = this.gameObject.transform.rotation;

        //secondPin.gameObject.transform.rotation = Quaternion.Euler(-90, 90, 0);

        float offset = this.transform.localScale.x;
        #region added line
        MeshFilter mf = GetComponentInChildren<MeshFilter>();//.mesh.bounds.size
        Vector3 size = mf.mesh.bounds.size;
        float max = Mathf.Max(size.x, size.y, size.z);

        offset = max * 4f;
        #endregion
        // for demo project, secondPinOffsetMultiplier is = 1.5f
        newPos = new Vector3(this.gameObject.transform.position.x - secondPinOffsetMultiplier * offset,
                             this.gameObject.transform.position.y,
                             this.gameObject.transform.position.z);
        secondPin.gameObject.transform.position = newPos;

        secondPinConnDetails = secondPin.GetComponent<PinConnectionDetails>();

        secondPinConnDetails.UpdatePinConnDetails(true, thisPinConnectedConnector);
        secondPinConnDetails.ApplyFixingConstraints();
        secondPinConnDetails.isThisIndirectConnPin = true;
        
        Invoke("UpdatePosAgain", 0.5f);
    }

    void UpdatePosAgain()
    {
        //secondPin.gameObject.transform.rotation = Quaternion.Euler(-90, 90, 0);

        secondPin.gameObject.transform.position = newPos;
        secondPin.gameObject.GetComponent<Collider>().enabled = true;
        secondPin.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        secondPin.transform.parent = (this.gameObject.transform);
    }

    #endregion
}
