using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionAcceptor_new : MonoBehaviour
{
    #region public non serialized variables

    //[NonSerialized]
    public bool isThisConnectorConnected;
    //[NonSerialized]
    public bool isSecondConnDone;

    //[NonSerialized]
    public GameObject thisConnConnectedPin;

    #endregion

    #region public variables   

    public Vector3 posOffsetValues;

    public float yOffsetForPin;

    public int maxNoOfConnections;

    public float yOffsetForPinRemoval;

    public Vector3 rotOffsetAngles;

    #endregion

    #region private variables

    private Vector3 newPos;
    private PinConnectionDetails pinConnDetails;
    private Rigidbody pinRigidBody;
    private DragPin pinDragPin;
    private Collider pinCollider;

    #endregion

    void Start()
    {
        isThisConnectorConnected = false;
        //newPos = new Vector3(posOffsetValues.x, posOffsetValues.y, posOffsetValues.z);
    }

    private void Awake()
    {
        temp = true;
    }

    #region connection checking and status updating functions

    private void OnCollisionEnter(Collision collision)
    {
        if (!isThisConnectorConnected && collision.gameObject.tag.Contains("Pin"))
        {
            temp = false;
            isThisConnectorConnected = true;
            thisConnConnectedPin = collision.gameObject;

            pinDragPin = collision.gameObject.GetComponent<DragPin>();
            pinDragPin.shallIUpdatePos = false;

            pinCollider = collision.gameObject.GetComponent<Collider>();
            Debug.Log("Disabling Collider");
            pinCollider.enabled = false;

            pinRigidBody = collision.gameObject.GetComponent<Rigidbody>();
            pinRigidBody.useGravity = false;
            pinRigidBody.isKinematic = true;

            newPos = new Vector3(this.gameObject.transform.position.x,
                                 this.gameObject.transform.position.y + yOffsetForPin,
                                 this.gameObject.transform.position.z);


            //if (thisConnConnectedPin.transform.Find("follow") != null)
            //{
            //    make_pin_orientation();
            //}
            //else
            //{
            thisConnConnectedPin.transform.rotation = Quaternion.Euler(0, 0, 0);
            thisConnConnectedPin.transform.Rotate(rotOffsetAngles.x, rotOffsetAngles.y, rotOffsetAngles.z);
            thisConnConnectedPin.transform.position = newPos;
            thisConnConnectedPin.transform.Translate(posOffsetValues, Space.Self);

            //}
            pinConnDetails = collision.gameObject.GetComponent<PinConnectionDetails>();
            pinConnDetails.UpdatePinConnDetails(true, this.gameObject);
            pinConnDetails.ApplyFixingConstraints();

            if (pinConnDetails.secondPin != null)
            {
                pinConnDetails.SecondPinStateSetter();
            }
            Invoke("UpdatePosAgain", 0.1f);
        }
    }


    void make_pin_orientation()
    {
        Transform follow = thisConnConnectedPin.transform.Find("follow");

        Follow follow_script = thisConnConnectedPin.GetComponent<Follow>();

        follow.position = new Vector3(this.gameObject.transform.position.x,
                                 this.gameObject.transform.position.y + yOffsetForPin,
                                 this.gameObject.transform.position.z);
        //follow.transform.right = -transform.forward;
        //follow_script.move_parent_relative_tochild();
        //follow_script.update_location_and_rotation();
        follow_script.follow_child_func();

    }
    void UpdatePosAgain()
    {
        if (isThisConnectorConnected && thisConnConnectedPin != null)
        {
            thisConnConnectedPin.transform.position = thisConnConnectedPin.transform.position;
            thisConnConnectedPin.transform.position = thisConnConnectedPin.transform.position;
            thisConnConnectedPin.transform.rotation = thisConnConnectedPin.transform.rotation;
            //Debug.Log("Disabling Collider");
            pinCollider.enabled = true;
            //pinRigidBody.isKinematic = false;
            Invoke("EnableCollider", 0.1f);
        }
    }

    void EnableCollider()
    {
        //Debug.Log("Enabling Collider again");
        pinCollider.enabled = true;
        pinRigidBody.isKinematic = false;
    }

    public void ResetDetails()
    {
        //Debug.Log(this.name + " resetting");
        isThisConnectorConnected = false;
        thisConnConnectedPin = null;
    }

    public bool temp;

    /*
    private void Update()
    {
        if (temp)
        {
            //Debug.Log("Laka Laka Laka Laka");
            ResetDetails();
        }
    }
    */

    private void OnCollisionExit(Collision collision)
    {
        temp = true;
        ResetDetails();
    }

    #endregion
}
