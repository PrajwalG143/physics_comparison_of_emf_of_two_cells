using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererPosSetter : MonoBehaviour
{
    private LineRenderer cable;
    public GameObject pointOne;
    public GameObject pointTwo;
    public Material cableMat;
    // Start is called before the first frame update
    void Start()
    {
        cable = this.GetComponent<LineRenderer>();
        cable.positionCount = 2;
        cable.SetPosition(0, pointOne.transform.position);
        cable.SetPosition(1, pointTwo.transform.position);
        cable.startWidth = 0.02f;
        cable.endWidth = 0.02f;
        cable.material = cableMat;
    }

    // Update is called once per frame
    void Update()
    {
        cable.SetPosition(0, pointOne.transform.position);
        cable.SetPosition(1, pointTwo.transform.position);
    }
}
