using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragPin : MonoBehaviour
{
    private Vector3 screenPoint;
    private Vector3 offset;

    public bool shallIUpdatePos = false;
    public bool isDragNow = false;
    
    public static bool isObjectBeingDragged = false;
    #region Added 
    public bool isPair_connected = false;
    #endregion

    private void Start()
    {
        this.gameObject.GetComponent<Rigidbody>().freezeRotation = true;
        isObjectBeingDragged = false;

        //Added
        isPair_connected = false;
    }

    public bool isThisPinBeingDragged = false;

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        this.gameObject.GetComponent<Rigidbody>().useGravity = false;
        isDragNow = false;
        shallIUpdatePos = false;
        isThisPinBeingDragged = false;

        if (isPair_connected == false)
        {
            if (this.GetComponent<PinConnectionDetails>().isThisPinConnected && !this.gameObject.GetComponent<PinConnectionDetails>().isThisIndirectConnPin)
            {
                this.gameObject.GetComponent<PinConnectionDetails>().StartCoroutineMPU(0.01f);
            }

            else if (this.gameObject.GetComponent<PinConnectionDetails>().isThisIndirectConnPin)
            {
                this.gameObject.GetComponent<PinConnectionDetails>().StartCoroutineMPR(0.01f);
            }
        }
    }

    void OnMouseDrag()
    {
        if (shallIUpdatePos && this.GetComponent<PinConnectionDetails>().secondPin == null)
        {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            this.GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(transform.position, curPosition,0.5f));
            isObjectBeingDragged = true;
            isThisPinBeingDragged = true;
        }
    }

    private void Update()
    {
        if (!shallIUpdatePos)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.name == this.name)
                {
                    shallIUpdatePos = true;
                }
            }
        }
    }

    private void OnMouseUp()
    {
        if (shallIUpdatePos && !this.GetComponent<PinConnectionDetails>().isThisPinConnected)
        {
            this.gameObject.GetComponent<Rigidbody>().useGravity = true;
            if(this.gameObject.GetComponent<PinConnectionDetails>().secondPin != null)
            {
                this.gameObject.GetComponent<PinConnectionDetails>().secondPin.GetComponent<Rigidbody>().useGravity = true;
            }
            isDragNow = false;
            shallIUpdatePos = false;
            isObjectBeingDragged = false;
            isThisPinBeingDragged = false;
        }
    }
}
