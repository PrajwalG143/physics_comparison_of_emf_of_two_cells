using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class calculate_Average_ratio : MonoBehaviour
{

   

    //public Transform text_parent;

    public TMP_Text avg_text;

    public Button avg_button;

    public TMP_Text avg_obaservation_l1_l2_ratio;
    // Start is called before the first frame update
    void Start()
    {
       
    }


    public void update_average()
    {
        float sum = 0;
        int not_zero_r_null = 0;
       foreach(Transform child in transform)
        {
            TMP_Text txt = child.GetComponent<TMP_Text>();
            float number=0;
            bool success = float.TryParse(txt.text, out number);
            if (success)
            {
                sum += number;

                if(number>0)
                {
                    not_zero_r_null++;
                }
            }
            else
            {
               print($"Attempted conversion of '{number}' failed.");
            }
        }
        float ratio = (sum / not_zero_r_null);
        avg_text.text = ratio.ToString("0.##");

        if(avg_obaservation_l1_l2_ratio!=null)
        {
            avg_obaservation_l1_l2_ratio.text = avg_text.text;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
