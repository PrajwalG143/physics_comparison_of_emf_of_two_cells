using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ConnectionManagerCustom : MonoBehaviour
{

    public TextMeshPro UI_text,conn_text;
    public string UItxt;
    public int connection_count;
    private static ConnectionManagerCustom _connManagCust_myInstance;

    public int totalProperConnectionsDone;
    public static ConnectionManagerCustom ConnManCustMyInstance
    {
        get => _connManagCust_myInstance;
    }

    [Serializable]
    public  struct ValidConnectionPair
    {
        public GameObject pointOne;
        public GameObject pointTwo;
    }


    public ValidConnectionPair[] properConnectionPairs;
    private bool isCorrectConnIdentified = false;

    private void Awake()
    {
        if (_connManagCust_myInstance == null)
        {
            _connManagCust_myInstance = this;
        }
        else
        {
            Debug.Log("Myinstance is already existing");
        }
    }


    public bool CheckConnection(GameObject object1, GameObject object2)
    {
        isCorrectConnIdentified = false;

        foreach (ValidConnectionPair obj in properConnectionPairs)
        {
            if ((obj.pointOne == object1 && obj.pointTwo == object2) || (obj.pointOne == object2 && obj.pointTwo == object1))
            {
                isCorrectConnIdentified = true;
                break;
            }

            else
            {
                continue;
            }
        }
        if (isCorrectConnIdentified)
        {

            UpdateUIText("Checked cable connection is valid");
            totalProperConnectionsDone++;

            if (conn_text != null)
                conn_text.text = totalProperConnectionsDone.ToString();
        }
        else
        {
            UpdateUIText("Checked cable connection is invalid");
            connection_count = totalProperConnectionsDone;
            //if (count != null)
            //    count.text = totalProperConnectionsDone.ToString();
        }

        if (totalProperConnectionsDone == properConnectionPairs.Length)
        {
            if (UI_text != null)
                UI_text.text = "Cable connections completed";
        }
        return isCorrectConnIdentified;
    }

    public void UpdateUIText(string inputData)
    {
        if (UI_text != null)
            UI_text.text = inputData;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
