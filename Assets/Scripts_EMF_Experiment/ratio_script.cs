using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ratio_script : MonoBehaviour
{
    public TMP_InputField l1_ipField, l2_ipField;

    public TMP_Text obsevation_l1, observation_l2,observation_l1_l2_ratio;
    private bool l1_changed, l2_changed;
    TMP_Text l1_tmpTXT, l2_tmpTXT;

    TMP_Text tmp_text;

    // Start is called before the first frame update
    void Start()
    {
        if(l1_ipField!=null)
        l1_ipField.onValueChanged.AddListener(l1_updated);

        if(l2_ipField!=null)
        l2_ipField.onValueChanged.AddListener(l2_updated);

        //l1_tmpTXT = l1_ipField.text;GetChild(0).GetChild(1).GetComponentInChildren<TMP_Text>();
        //l2_tmpTXT = l2_ipField.transform.GetChild(0).GetChild(1).GetComponentInChildren<TMP_Text>();
        tmp_text = GetComponent<TMP_Text>();
    }

    void l1_updated(string l1_txt)
    {
        if(obsevation_l1!=null)
        {
            obsevation_l1.text = l1_txt;
        }

        if(l2_ipField!=null)
        if (!string.IsNullOrEmpty(l1_txt) && !string.IsNullOrEmpty(l2_ipField.text))
        {
            float l1 = float.Parse(l1_txt);
            float l2 = float.Parse(l2_ipField.text);

            if (l1 > 0 && l2 > 0)
            {

                tmp_text.text = (l1 / l2).ToString("0.##");
                    if(observation_l1_l2_ratio!=null)
                        observation_l1_l2_ratio.text = tmp_text.text;
            }
        }
        else
        {
                tmp_text.text = "";
                if (observation_l1_l2_ratio != null)
                    observation_l1_l2_ratio.text = tmp_text.text;
            }
    }

    void l2_updated(string l2_txt)
    {
        if(observation_l2!=null)
        {
            observation_l2.text = l2_txt;
        }

        if(l1_ipField!=null)
        if (!string.IsNullOrEmpty(l1_ipField.text) && !string.IsNullOrEmpty(l2_txt))
        {

            float l1 = float.Parse(l1_ipField.text);
            float l2 = float.Parse(l2_txt);

            if (l1 > 0 && l2 > 0)
            {

                tmp_text.text = (l1 / l2).ToString("0.##");
                    if (observation_l1_l2_ratio != null)
                        observation_l1_l2_ratio.text = tmp_text.text;


                }
        }
        else
        {
                tmp_text.text = "";
                if (observation_l1_l2_ratio != null)
                    observation_l1_l2_ratio.text = tmp_text.text;
            }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
