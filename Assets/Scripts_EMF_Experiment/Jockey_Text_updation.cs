using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class Jockey_Text_updation : MonoBehaviour
{

    private Camera camera_to_lookAt;
    public TMPro.TextMeshPro text_3D;

    #region mapping values
    public float left_local_pos_val= -0.5103861f, right_local_pos_val= 0.4896139f;
    private float total_dis_val,mul_factor;
    private Vector3 init_localPos;
    #endregion

    //scale length one side
    //[SerializeField]
    public float scale_length = 100;

    //Wire length dragged length of jockey
    public float length_of_wire;

    public static LeanDragTranslateAlong jockey_translateAlong_script;

   static bool all_connected_see_for_jockey_pin = false;
    //public static float length_static=0;
    // Start is called before the first frame update
    void Start()
    {
        camera_to_lookAt = Camera.main;
        total_dis_val = right_local_pos_val - left_local_pos_val;

        init_localPos = transform.localPosition;

        //***********Mapping the Values of Jockey movement to Meter Scale Values**********************//

        //100 is length of the scale

        mul_factor = scale_length / total_dis_val;

        jockey_translateAlong_script = GetComponent<LeanDragTranslateAlong>();

        //jockey_translateAlong_script.enabled = false;

        all_connected_see_for_jockey_pin = false;
    }

    public static void jockey_movt_enable(bool boolean=false)
    {
        all_connected_see_for_jockey_pin = boolean;
        //jockey_translateAlong_script.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        #region All connection verified for movement
        //all_connected_see_for_jockey_pin == true
        //if (ConnectionManager.MyInstance.allConnected == true && ConnectionManager.MyInstance.complete_connection_with_jockey == true)
        //{
        //   if( jockey_translateAlong_script.enabled == false)
        //    jockey_translateAlong_script.enabled = true;
        //}
        //else
        //{
        //    if(jockey_translateAlong_script.enabled==true)
        //    jockey_translateAlong_script.enabled = false;
        //}
        #endregion
        if (text_3D!=null)
        {
            text_3D.transform.forward = camera_to_lookAt.transform.forward;

        if(transform.hasChanged)
        {
            transform.hasChanged = false;
            update_text();
           //to rotate the galvanometer needle 
           Galvanometer_Deflection.move_needle = true;
        }
        }
    }

    void update_text()
    {


        float dis = Mathf.Abs(transform.localPosition.x-init_localPos.x);

        //***********Mapping the Values of Jockey movement to Meter Scale Values**********************//
        float   val = dis * mul_factor;

        //if the Jockey is in 2nd Wire 
        /*
         * WE NEED TO CALCULATE DISTANCE FROM RIGHT MOST END 
         * THE JOCKEY WILL HAVE TRAVERSED 100CM DISTANCE
         * SO WE WILL ADD 100 TO DISTANCE TRAVERSED
         */
        if(transform.localPosition.z>init_localPos.z+(0.0086F))
        {
            dis = Mathf.Abs(right_local_pos_val - transform.localPosition.x);

            val = scale_length + dis * mul_factor;

        }

        length_of_wire = val;

        if(text_3D!=null)
        text_3D.text = val.ToString("0.##") + " cm";
    }

    private void OnMouseDrag()
    {
        //Galvanometer_Deflection.move_needle = true;
    }
    //public void selectDetect()
    //{
    //    Galvanometer_Deflection.move_needle = true;
    //}
}
