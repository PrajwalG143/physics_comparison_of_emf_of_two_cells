using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class length_update_in_Table : MonoBehaviour
{

    public Transform text_parent;

    public int ind = 0,max_ind=3;

    public Jockey_Text_updation jockey_Text_Updation_script;

    public List<float> lengths;

    public static TMPro.TMP_Text tmp;

    public  TMPro.TMP_Text tmpro;
    public static bool clear;

    public Transform average_button;
    // Start is called before the first frame update
    void Start()
    {
        ind = 0;
        lengths = new List<float>();
    }

    public void update_length()
    {
        tmpro=  text_parent.GetChild(ind).GetComponent<TMPro.TMP_Text>();

        tmp = tmpro;
        if(jockey_Text_Updation_script!=null&&tmpro!=null)
        {
            tmpro.text = jockey_Text_Updation_script.length_of_wire.ToString("0.##");
            lengths.Add(jockey_Text_Updation_script.length_of_wire);
        }
        
        //if(clear==true)
        //{ 
        
        //}
        //else
        ind++;
        if(ind>max_ind-1)
        {
            ind = 0;
            if(average_button!=null)
            {
                average_button.gameObject.SetActive(true);
            }
            lengths.Clear();

        }
        
    }



    public void clear_text()
    {
        tmp.text = "0";
        clear = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
