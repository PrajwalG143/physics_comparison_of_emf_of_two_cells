using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steps_Count_script : MonoBehaviour
{
    public  int step_no;

    public Transform Instruction_ScreenSpace;
    // Start is called before the first frame update
    void Start()
    {
        step_no = 0;
        Instruction_ScreenSpace = this.transform;
    }

   public void increment_step_no()
    {
        step_no++;
        step_no= Mathf.Clamp(step_no,0, Instruction_ScreenSpace.childCount-1);
        enable_instruction(step_no);
    }

   public void decrement_step_no()
    {
        step_no--;
        step_no=  Mathf.Clamp(step_no, 0, Instruction_ScreenSpace.childCount-1);
        enable_instruction(step_no);
    }

    public void enable_instruction(int index_instruction)
    {
        for (int i = 0; i < Instruction_ScreenSpace.childCount; i++)
        {
            if (i == index_instruction)
            {
                Instruction_ScreenSpace.GetChild(i).gameObject.SetActive(true);
            }
            else
            {
                Instruction_ScreenSpace.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
