using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_constraint_movement : MonoBehaviour
{
    public Transform cylinder;
    public bool isselectedtrue = false;
    public float maxval = 0f, minval = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    // Update is called once per frame
    void Update()
    {
        if(cylinder != null && isselectedtrue)
        {
            if(cylinder.localPosition.x <= minval)
            {
                cylinder.localPosition = new Vector3(minval, cylinder.localPosition.y, cylinder.localPosition.z);
            }
            if (cylinder.localPosition.x >= maxval)
            {
                cylinder.localPosition = new Vector3(maxval, cylinder.localPosition.y, cylinder.localPosition.z);
            }
        }
    }

    

    public void isselected()
    {
        isselectedtrue = true;
    }   public void dselected()
    {
        isselectedtrue = false;
    }
}
