using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class create_new_pins : MonoBehaviour
{

    public Transform pin1, pin2;
    private Vector3 pos1, pos2;
    private static Vector3 pos_wire;
    // Start is called before the first frame update
    void Start()
    {
        pos_wire = this.transform.position;
        pos1 = pin1.position;
        pos2 = pin2.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(pin1.transform.position!=pos1&& pin2.transform.position!=pos2)
        {
           

            newWire(this.transform);
        }
    }

    public static void newWire(Transform tr)
    {
       Transform new_trns= Instantiate(tr);
        new_trns.position = pos_wire;
        
        //new_trns.position = tr.position;
    }
}
