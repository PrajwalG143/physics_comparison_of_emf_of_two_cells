using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinconncetionDetailsCustom : MonoBehaviour
{


    public GameObject thisPinConnectedConnector;

    [NonSerialized]
    public bool isThisPinConnected;

    [NonSerialized]
    public bool isProperConnCountIncremented;
    #region second Pin
    public GameObject thisPairOtherPin;

    [NonSerialized]
    public bool secondPinConnected;

    PinconncetionDetailsCustom pinconncetionDetailsCustomSecondPin_script;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        isThisPinConnected = false;
        isProperConnCountIncremented = false;

        if (thisPairOtherPin!=null)
        {
            pinconncetionDetailsCustomSecondPin_script = thisPairOtherPin.GetComponent<PinconncetionDetailsCustom>();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.GetComponent<ConnectionAcceptorCustom>() != null)
        {
            if (!isThisPinConnected && thisPinConnectedConnector == null)
            {
                thisPinConnectedConnector = collision.gameObject;
                UpdatePinConnectionDetails(false);
            }
        }
    }
    public void UpdatePinConnectionDetails(bool _isPinConnected)
    {
        isThisPinConnected = true;

        //if(pinconncetionDetailsCustomSecondPin_script.)
        if (isProperConnCountIncremented == false && pinconncetionDetailsCustomSecondPin_script.isProperConnCountIncremented == false)
            if (isThisPinConnected && pinconncetionDetailsCustomSecondPin_script.isThisPinConnected)
            {
                if (ConnectionManagerCustom.ConnManCustMyInstance.CheckConnection(thisPinConnectedConnector, pinconncetionDetailsCustomSecondPin_script.thisPinConnectedConnector))
                {
                    isProperConnCountIncremented = true;
                    pinconncetionDetailsCustomSecondPin_script.isProperConnCountIncremented = true;
                }
            }

    }
    private void OnCollisionStay(Collision collision)
    {
        //if(thisPinConnectedConnector==null)
        //if (collision.transform.GetComponent<ConnectionAcceptorCustom>() != null)
        //{
        //    thisPinConnectedConnector = collision.gameObject;
        //    isThisPinConnected = true;

        //    UpdatePinConnectionDetails();
        //}
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.GetComponent<ConnectionAcceptorCustom>() != null)
        {
            if (thisPinConnectedConnector != null)
                thisPinConnectedConnector = null;
            if (isThisPinConnected)
                ResetPinConnDetails(false);
        }
        //isThisPinConnected = false;
        //if(isThisPinConnected)
        //isProperConnCountIncremented = false;
    }

    public void ResetPinConnDetails(bool _isThisPinRemoved)
    {
        if (isThisPinConnected)
        {
            isThisPinConnected = _isThisPinRemoved;
            thisPinConnectedConnector = null;
         
            if (pinconncetionDetailsCustomSecondPin_script.isThisPinConnected)
            {
                ConnectionManager.MyInstance.UpdateUIText("This pin is disconnected, but the other pin is connected");
                if (isProperConnCountIncremented && pinconncetionDetailsCustomSecondPin_script.isProperConnCountIncremented)
                {
                    isProperConnCountIncremented = false;
                    pinconncetionDetailsCustomSecondPin_script.isProperConnCountIncremented = false;
                    ConnectionManager.MyInstance.totalProperConnectionsDone--;
                   /* if (ConnectionManager.MyInstance.count.text != null)
                        ConnectionManager.MyInstance.count.text = ConnectionManager.MyInstance.totalProperConnectionsDone.ToString();*/
                }
            }
            else
            {
                //ConnectionManager.MyInstance.UpdateUIText("This pin disconnected, other pin also disconnected... Entire wire is disassembled now");
            }
        }
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }
}
