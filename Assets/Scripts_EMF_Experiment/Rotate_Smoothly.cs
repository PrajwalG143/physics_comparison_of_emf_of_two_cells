using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate_Smoothly : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(Rotate(new Vector3(180,90,90)));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   public IEnumerator Rotate(Vector3 rot)
    {
        Quaternion startRotation = transform.rotation;
        float endZRot = 405f;
        float duration = 1f;
        float t = 0;

        while (t < 1f)
        {
            t = Mathf.Min(1f, t + Time.deltaTime / duration);
            Vector3 newEulerOffset = rot*t;// Vector3.forward * (endZRot * t);
            // global z rotation
            transform.rotation = Quaternion.Euler(newEulerOffset) * startRotation;
            // local z rotation
            // transform.rotation = startRotation * Quaternion.Euler(newEulerOffset);
            yield return null;
        }
    }
}
