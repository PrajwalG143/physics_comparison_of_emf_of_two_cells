using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E1orE2_detector : MonoBehaviour
{
    [Tooltip("plug keys and Terminals in order")]
    public  Transform P1, P2, T1, T2, T3;


    public bool coll_exit_pk1=false,coll_exit_pk2;
    public Transform contact_plugKey;

    public Galvanometer_Deflection galvanometer_Deflection_script;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //if (collision.collider.transform == P1)
        //{
        //    coll_exit_pk1 = false;

        //}
        //else if (collision.collider.transform == P2)
        //{
        //    coll_exit_pk2 = false;
        //}

        //FOREACH HERE

        #region COLLISION STAY OF PINS


        foreach (ContactPoint cp in collision.contacts)
        {
            if (cp.otherCollider.transform == P1)
            {
                coll_exit_pk1 = false;
            }
            else if (cp.otherCollider.transform == P2)
            {
                coll_exit_pk2 = false;
            }
        }

        if (coll_exit_pk1 == false && coll_exit_pk2 == false)
        {
            contact_plugKey = null;
            if (galvanometer_Deflection_script != null)
            {
                galvanometer_Deflection_script.set_mul_factor();
                Galvanometer_Deflection.move_needle = true;
            }
        }


        #endregion

    }

    private void OnCollisionExit(Collision collision)
    {
       if( collision.collider.transform==P1)
        {
            coll_exit_pk1 = true;

            contact_plugKey = P2;
            if (galvanometer_Deflection_script != null)
            {
                galvanometer_Deflection_script.set_mul_factor();
                Galvanometer_Deflection.move_needle = true;
            }
        }
        else if (collision.collider.transform==P2)
        {
            coll_exit_pk2 = true;

            contact_plugKey = P1;
            if (galvanometer_Deflection_script != null)
            {
                galvanometer_Deflection_script.set_mul_factor();
                Galvanometer_Deflection.move_needle = true;
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        #region COLLISION STAY OF PINS
        //if (coll_exit_pk1 || coll_exit_pk2)
        //{

        //    //List<ContactPoint> li = new List<ContactPoint>();
        //    //li.AddRange(collision.contacts);

        //    foreach (ContactPoint cp in collision.contacts)
        //    {
        //        if (coll_exit_pk1)
        //        {
        //            if(cp.otherCollider.transform==P2)
        //            {
        //                contact_plugKey = cp.otherCollider.transform;

        //                if(galvanometer_Deflection_script!=null)
        //                galvanometer_Deflection_script.set_mul_factor();
        //            }
                   
        //        }
        //        else
        //        {
        //            if (cp.otherCollider.transform==P1)
        //            {
        //                contact_plugKey = cp.otherCollider.transform;

        //                if(galvanometer_Deflection_script!=null)
        //                galvanometer_Deflection_script.set_mul_factor();
        //            }

        //        }
        //    }


        //}
        #endregion

    }
    // Update is called once per frame
    void Update()
    {
        //if(coll_exit_pk1==false &&coll_exit_pk2==false)
        //{
        //    contact_plugKey = null;
        //}
    }
}
