using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rheostat_resistance : MonoBehaviour
{
    #region resistance values
    [Header("Resistance Settings")]
    //[TextArea]
    [Tooltip("Enter maximum value of resistance of Rheostat")]
    public float max_resistance=1000,initial_res=400;
    [Tooltip("Enter minimum value of resistance of Rheostat")]
    public float resistance_value=0;
    #endregion
    #region positions
    private Vector3 init_locPos;
   [SerializeField]
    private float left_most_val = -1.253918f, right_most_val = 1.268466f;

    
    private float range_val_mov, mul_factor;
    #endregion

    #region lengths of wire
    public float l2_max=190;

    public float l1,l2;
    #endregion


    #region batteries and values

    public float E1=1.08f, E2=1.5f;

    #endregion
    [SerializeField]
   private Galvanometer_Deflection galvanometer_Deflection_script;
    // Start is called before the first frame update
    void Start()
    {
        init_locPos = transform.localPosition;
        range_val_mov = Mathf.Abs(right_most_val - left_most_val);

        //***********Mapping the Values of cylinder movement to resistance Values**********************//

       
        mul_factor = (max_resistance-initial_res) / range_val_mov;

        map_res_value();
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.hasChanged)
        {
            transform.hasChanged = false;
            //resistance should be mapped when its moved
            map_res_value();

            //The multiplying factor for needle deflecion angle has to change when the resistance is varied
            galvanometer_Deflection_script.set_mul_factor();

            //deflect the galvanometer
            Galvanometer_Deflection.move_needle = true;
        }
    }

    void map_res_value()
    {

        float dis = Mathf.Abs(transform.localPosition.x - init_locPos.x);



        resistance_value = dis * mul_factor + initial_res;

        map_length_with_res(resistance_value);
    }

    void map_length_with_res(float res)
    {
        float len = 0;

        len = (l2_max / max_resistance)*res;

        l2 = len;

        l1 = (E1 / E2) * l2;
    }

    //public void selectDetect()
    //{
    //    Galvanometer_Deflection.move_needle = true;
    //}
    private void OnMouseDrag()
    {
        //Galvanometer_Deflection.set_mul_factor_res = true;
        //Galvanometer_Deflection.move_needle = true;
    }
}
