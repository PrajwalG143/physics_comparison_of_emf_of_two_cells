using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constraint_Rheostat_cylinder : MonoBehaviour
{
    public GameObject spiral_constraint_1, spiral_constraint_2,
      cylinder_contraint_1, cylinder_contraint_2;

    private Vector3 sp1, sp2,c1,c2,cyl_sp1_pos,cyl_sp2_pos;
    private bool s1set,s2set;
    // Start is called before the first frame update
    void Start()
    {
        sp1 =transform.parent.InverseTransformPoint( spiral_constraint_1.transform.position);
        sp2= transform.parent.InverseTransformPoint(spiral_constraint_2.transform.position);

        c1 = transform.parent.InverseTransformPoint(cylinder_contraint_1.transform.position);
        c2 = transform.parent.InverseTransformPoint(cylinder_contraint_2.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if(c1.x>=sp1.x)
        {
            if (Mathf.Round( c1.x )== Mathf.Round(sp1.x))
            {
            cyl_sp1_pos = transform.position;
                s1set = true;
            }

         if(c1.x >sp1.x&&s1set)
        {
            //cyl_1_pos = transform.position;
            transform.position = cyl_sp1_pos;
        }
        }
        if (c2.x <= sp2.x)
        {
            if (Mathf.Round(c2.x) == Mathf.Round(sp2.x))
            {
                cyl_sp2_pos = transform.position;
                s2set = true;
            }
        if (c2.x < sp2.x &&s2set)
        {
            //cyl_1_pos = transform.position;
            transform.position = cyl_sp2_pos;
        }
        }

        
    }

}
