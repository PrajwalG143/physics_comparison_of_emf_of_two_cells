using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jockey_Tip_follow : MonoBehaviour
{
    Follow follow_child_script;
    Transform follow_child;
    // Start is called before the first frame update
    void Start()
    {
        follow_child_script = GetComponent<Follow>();
        follow_child = follow_child_script.follow_child;
    }

    // Update is called once per frame
    void Update()
    {
        if(follow_child!=null)
        {
          if(  follow_child.hasChanged)
            {
                follow_child.hasChanged = false;
                follow_child_script.follow_child_func();
            }
        }
    }
}
