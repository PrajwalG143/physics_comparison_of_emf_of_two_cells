using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constraint_Motion_cylinder : MonoBehaviour
{
    public Transform cylinder;
    public bool isselectedtrue = false;
    public float maxval = 0f, minval = 0f;
    public Transform spiral_const_1, spiral_const_2,cylinder_loc1, cylinder_loc2;

    private float dis_c1_cylPos_x, dis_c2_cylPos_x;
    // Start is called before the first frame update
    void Start()
    {
        cylinder = this.transform;
        maxval = transform.parent.InverseTransformPoint(spiral_const_1.position).x;
        minval = transform.parent.InverseTransformPoint(spiral_const_2.position).x;

        dis_c1_cylPos_x = Mathf.Abs(Mathf.Abs(transform.localPosition.x) - Mathf.Abs(maxval));
        dis_c2_cylPos_x=Mathf.Abs(Mathf.Abs(transform.localPosition.x) - Mathf.Abs(minval));

    }

    // Update is called once per frame
    void Update()
    {

        float c_loc1_x= transform.parent.InverseTransformPoint(cylinder_loc1.transform.position).x, c_loc2_x= transform.parent.InverseTransformPoint(cylinder_loc2.transform.position).x;   

        if (cylinder != null )
        {
            if (c_loc2_x <= minval)
            {
                cylinder.localPosition = new Vector3(minval+dis_c2_cylPos_x, cylinder.localPosition.y, cylinder.localPosition.z);
            }
            if (c_loc1_x >= maxval)
            {
                cylinder.localPosition = new Vector3(maxval-dis_c1_cylPos_x, cylinder.localPosition.y, cylinder.localPosition.z);
            }
        }
    }



    public void isselected()
    {
        isselectedtrue = true;
    }
    public void dselected()
    {
        isselectedtrue = false;
    }
}
