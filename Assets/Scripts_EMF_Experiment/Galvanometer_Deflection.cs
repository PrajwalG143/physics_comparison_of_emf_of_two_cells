using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Galvanometer_Deflection : MonoBehaviour
{
    enum plugKeyNames
    {
        plugKey1,
        plugKey2,

    }

    public  Jockey_Text_updation jockey_Text_Updation_script;

    public Rheostat_resistance rheostat_Resistance_script;

    public E1orE2_detector e1OrE2_Detector_script;

    public Transform needle_of_galvanometer;
    [SerializeField]
    private Transform rheostat_bar,jockey_tip;

   
    private float max_angle_rance_of_galv=83;//85-2

    float ang = 0;

    float mul_factor = 0;

    Vector3 init_angles;

    public static bool move_needle = false,set_mul_factor_res=false;
    // Start is called before the first frame update
    void Start()
    {
        jockey_tip = jockey_Text_Updation_script.transform;
        rheostat_bar = rheostat_Resistance_script.transform;

        set_mul_factor();
        //l1 or l2

      init_angles=   needle_of_galvanometer.transform.localEulerAngles;
     }

   public void set_mul_factor()
    {
        float half_angle_for_zero_deflection = max_angle_rance_of_galv / 2;

        float length = 200f;


        Transform pk_contact = e1OrE2_Detector_script.contact_plugKey;
        if (pk_contact != null)
        {
            if (rheostat_Resistance_script != null)
            {
                if (pk_contact == e1OrE2_Detector_script.P1)
                {
                    length = rheostat_Resistance_script.l1;
                }
                else if (pk_contact == e1OrE2_Detector_script.P2)
                {
                    length = rheostat_Resistance_script.l2;
                }
            }
        }
        else
        {
            if(jockey_Text_Updation_script!=null)
            length =(jockey_Text_Updation_script.scale_length-10);
        }
        mul_factor = half_angle_for_zero_deflection / length;// rheostat_Resistance_script.l2;// 200;

        if(e1OrE2_Detector_script.coll_exit_pk1==true&&e1OrE2_Detector_script.coll_exit_pk2==true)
        {
            mul_factor = 0;

        }
    }
    float map_values_angle(float mul_factor, float length)
    {
        float mapped_angle = 0;
        mapped_angle = (mul_factor *length) ;

        return mapped_angle;
    }


    private void LateUpdate()
    {
        
 
        if(rheostat_bar!=null)
        {
            //if(rheostat_bar.hasChanged)
            //{

                //rheostat_bar.hasChanged = false;


               //The multiplying factor has to change when the resistance is varied

                //set_mul_factor();
            //}


        //if (jockey_tip.hasChanged || rheostat_bar.hasChanged)
        if(jockey_tip!=null)
        if(move_needle &&ConnectionManager.MyInstance.allConnected == true && ConnectionManager.MyInstance.complete_connection_with_jockey == true)
        {
                    move_needle = false;
            //    jockey_Text_Updation_script.transform.hasChanged = false;

          //*****************The angle has to be changed when the resistance of rheostat or the jockey is moved***********************//

            ang = map_values_angle(mul_factor, jockey_Text_Updation_script.length_of_wire);

            ang = Mathf.Clamp(ang, 0, max_angle_rance_of_galv);

            needle_of_galvanometer.localRotation = Quaternion.Euler(init_angles.x,init_angles.y,init_angles.z+ ang);
        }
        }
    }
}
