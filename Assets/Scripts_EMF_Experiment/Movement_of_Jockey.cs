using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using Lean.Common;

public class Movement_of_Jockey : MonoBehaviour
{
    public bool isJockeySelected = false;
    private LeanManualTranslate leanManualTranslate_script;
    public int touch_count;
    public Transform wire;
    private Transform wire_1_loc, wire_2_loc;
    private float wire_1_loc_z, wire_2_loc_z;
    List<float> nos_z;
    private Bounds jockey_bounds;
    private Transform jockey_tip;
    // Start is called before the first frame update
    void Start()
    {
        leanManualTranslate_script = GetComponent<LeanManualTranslate>();
        wire_1_loc = wire.transform.GetChild(0);
        wire_2_loc = wire.transform.GetChild(1);
        wire_1_loc_z = wire_1_loc.transform.position.z;
        wire_2_loc_z=wire_2_loc.transform.position.z;

        nos_z = new List<float>();
        nos_z.Add(wire_1_loc_z);
        nos_z.Add(wire_2_loc_z);

        jockey_bounds = GetComponentInChildren<MeshFilter>().mesh.bounds;
        jockey_tip = transform.parent;
    }

    public void onJockeySelect()
    {
        isJockeySelected = true;

        touch_count++;
        if (touch_count == 1)
        {
            //StartCoroutine(Rotate(new Vector3(180, 90, 90)));
            //transform.rotation = Quaternion.Euler(new Vector3(180, 90, 90));
            //jockey_tip.transform.position=new Vector3(jockey_tip.transform.position.x, wire_1_loc.transform.position.y, jockey_tip.transform.position.z);
        }


    }

    public IEnumerator Rotate(Vector3 rot)
    {
        Quaternion startRotation = transform.rotation;
        float endZRot = 405f;
        float duration = 1f;
        float t = 0;

        while (t < 1f)
        {
            t = Mathf.Min(1f, t + Time.deltaTime / duration);
            Vector3 newEulerOffset = rot * t;// Vector3.forward * (endZRot * t);
            // global z rotation
            transform.rotation = Quaternion.Euler(newEulerOffset) * startRotation;
            // local z rotation
            // transform.rotation = startRotation * Quaternion.Euler(newEulerOffset);


            yield return null;
        }
    }
    public void onJockeyDeselect()
    {
        isJockeySelected = false;

        //if (leanManualTranslate_script != null && !isJockeySelected)
        //{
        //    leanManualTranslate_script.DirectionA = Vector3.right;
        //    leanManualTranslate_script.DirectionB = Vector3.up;
        //}
    }
    // Update is called once per frame
    void Update()
    {

       
    }
    //private void OnTriggerEnter(Collider other)
    //{
        
    //}

    private void OnTriggerEnter(Collider collision)
    {

        if (collision.transform.name == "wire")
        {
            Transform wire = collision.transform;

            Debug.Log("Touched Wire");



            if (leanManualTranslate_script != null)// && isJockeySelected)
            {
                leanManualTranslate_script.DirectionA = Vector3.right;
                leanManualTranslate_script.DirectionB = Vector3.right;

                leanManualTranslate_script.Space = Space.World;
            }

            //Access follow cript
            #region  Follow_part
            //Follow follow_script = GetComponent<Follow>();

            //Transform follow_child = follow_script.follow_child;


            if (wire_1_loc != null && wire_2_loc != null)
            {



                if (transform.parent != null)
                {
                    Transform tip = transform;//.parent;
                    float diff1 = Mathf.Abs(Mathf.Abs(tip.position.z) - Mathf.Abs(wire_1_loc_z));
                    float diff2 = Mathf.Abs(Mathf.Abs(tip.position.z) - Mathf.Abs(wire_2_loc_z));

                    float near = nearest(wire_1_loc_z, wire_2_loc_z, tip.transform.position.z);
                    if (tip)

                    {
                        tip.position.Set(tip.transform.position.x, tip.transform.position.y, near);
                        Vector3 pos = new Vector3(tip.transform.position.x, tip.transform.position.y, near);
                        tip.transform.rotation = Quaternion.Euler(0, 90, -90);
                        //StartCoroutine(Rotate_smoothly_transform(new Vector3(0, 90, -90), tip, pos));
                    }
                }
                //follow_child.transform.position.Set(follow_child.transform.position.x, follow_child.transform.position.y, near);

                //follow_child.transform.rotation = Quaternion.Euler(0, 0, -90);

                ////follow_script.follow_child_func();
                ////follow_script.update_location_and_rotation();
                ////follow_script.move_parent_relative_toChild();

                //transform.position.Set(follow_child.transform.position.x, follow_child.transform.position.y, near);

                //Transform prev_par_foll = follow_child.transform.parent;
                //if (prev_par_foll != null)
                //{
                //    follow_child.transform.parent = null;
                //   Transform prev_tr_par= transform.parent;
                //    transform.parent = follow_child;
                //    follow_child.transform.position.Set(follow_child.transform.position.x, follow_child.transform.position.y, near);

                //    transform.parent=prev_tr_par;
                //    follow_child.transform.parent= prev_par_foll;
                //}


                //float dis_bw_wire_joc=Mathf.Abs( wire_1_loc.transform.position.y-follow_child.transform.position.y);

                //transform.Translate(Vector3.up*dis_bw_wire_joc*10,Space.World);

            }
        

            #endregion

        }
    }

    public IEnumerator Rotate_smoothly_transform(Vector3 rot,Transform trns,Vector3 pos)
    {
        Quaternion startRotation = trns.rotation;
        float endZRot = 405f;
        float duration = 1f;
        float t = 0;

        while (t < 1f)
        {
            t = Mathf.Min(1f, t + Time.deltaTime / duration);
            Vector3 newEulerOffset = rot * t;// Vector3.forward * (endZRot * t);
            // global z rotation
            trns.rotation = Quaternion.Euler(newEulerOffset) * startRotation;
            // local z rotation
            // transform.rotation = startRotation * Quaternion.Euler(newEulerOffset);
            //trns.position.Set(trns.transform.position.x, trns.transform.position.y, near);
            yield return new WaitForEndOfFrame();
        }
        //trns.position.Set(trns.transform.position.x, trns.transform.position.y, near);
        trns.position.Set(pos.x,pos.y,pos.z);
    }
    public  float nearest(float w1, float w2,float n)
    {
        
        var val = Mathf.Abs(w1- n);
        var val2 = Mathf.Abs(w2 - n);

        return val == val2 ? 0 : (val < val2 ? w1 : w2);
    }
   

    public IEnumerator Rotate_smoothly(Vector3 rot)
    {
        Quaternion startRotation = transform.rotation;
        float endZRot = 405f;
        float duration = 1f;
        float t = 0;

        while (t < 1f)
        {
            t = Mathf.Min(1f, t + Time.deltaTime / duration);
            Vector3 newEulerOffset = rot * t;// Vector3.forward * (endZRot * t);
            // global z rotation
            transform.rotation = Quaternion.Euler(newEulerOffset) * startRotation;
            // local z rotation
            // transform.rotation = startRotation * Quaternion.Euler(newEulerOffset);
            yield return new WaitForEndOfFrame();
        }

       
    }
}
