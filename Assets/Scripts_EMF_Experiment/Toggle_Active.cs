using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toggle_Active : MonoBehaviour
{

    public GameObject go;
    public bool toggle_bool = false;
    // Start is called before the first frame update
    void Start()
    {
        toggle_bool = false;

        toggle_bool = go.activeSelf;
    }

    public void toggle_go()
    {
        if(toggle_bool==true)
        {
            toggle_bool = false;
        }
        else
        {
            toggle_bool = true;
        }

        go.SetActive(toggle_bool);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
