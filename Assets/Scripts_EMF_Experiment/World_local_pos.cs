using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class World_local_pos : MonoBehaviour
{
    public Vector3 world_pos, local_pos;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    //[ExecuteInEditMode]
    void Update()
    {
        world_pos=transform.position;
        local_pos=transform.localPosition;
        
    }
}
